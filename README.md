# L^2-Betti numbers and computability of reals

This project provides the Lean files delivered with the article "L^2-Betti numbers and computability of reals" by Clara Löh and Matthias Uschold. (arXiv:[2202.03159](https://arxiv.org/abs/2202.03159))

## Installation

For help with the installation, see installation-guidelines.txt

## License 

Copyright (c) 2022 Clara Löh and Matthias Uschold. All rights reserved.
Released under Apache 2.0 license as described in the file LICENSE.txt.
Authors: Clara Löh and Matthias Uschold.

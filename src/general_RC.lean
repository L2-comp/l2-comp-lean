/-
Copyright (c) 2022 Clara Löh and Matthias Uschold. All rights reserved.
Released under Apache 2.0 license as described in the file LICENSE.txt.
Authors: Clara Löh and Matthias Uschold.
-/

import tactic
import .general_lemmas

open classical 
open traced_algebra
open measure_theory
open set
open real


variables 
{matrix_ring: Type*} [ring_mat_ring: ring matrix_ring]
[algebra ℝ matrix_ring]
[tr_alg: traced_algebra ℝ matrix_ring] 
--the matrix is an algebra with trace over ℝ 
(A: matrix_ring) 
-- A is an element of the matrix ring

--we have a spectral measure μ of A       
{d:ℚ} (d_ge1: (d:ℝ)≥ 1)
(μ: measure ℝ) [finite_measure μ]
(μ_spec_meas: is_spectral_measure matrix_ring A μ d)

/-the polynomials that we want to work with-/
local notation 
  `pk` k  :=  (polynomial.C 1- polynomial.C(1/(d:ℝ)) * polynomial.X)^k
local notation 
  `char_seq` := (λ k, traced_algebra.tr (polynomial.eval₂ algebra.to_ring_hom A (pk k)))
/-the right computability is proved relative to the characteristic sequence-/


section general_RC 

--the characteristic sequence is above μ {0}

lemma char_seq_is_above 
  {μ: measure ℝ} [finite_measure μ]
  (μ_spec_meas: @is_spectral_measure matrix_ring ring_mat_ring _ tr_alg A μ d)
  (dge1: (d:ℝ)≥ 1)
	: ∀ (k:ℕ), (char_seq) k ≥ (μ {0}).to_real
:= begin 
    assume k:ℕ,

calc (char_seq) k   
    = ∫(x:ℝ) in Icc 0 d,  
        (λ (x : ℝ), polynomial.eval x (pk k)) x ∂μ
        : (μ_spec_meas (pk k)).symm
... = (λ (x : ℝ), polynomial.eval x (pk k)) 0 
        * ((μ {0}).to_real) + 
        ∫(x:ℝ) in 0..d,  
        (λ (x : ℝ), polynomial.eval x (pk k)) x ∂μ
        : by simp[int_Icc_vs_Ioc (d_nonneg dge1) μ (pkk_function_is_integrable_Icc μ k (d:ℝ))]
... = ((μ {0}).to_real) + 
        ∫(x:ℝ) in 0..d,  (λ (x : ℝ), polynomial.eval x (pk k)) x ∂μ
        : by simp   
... ≥  (μ {0}).to_real
        : by simp[interval_integral.integral_nonneg (d_nonneg dge1) 
                                                    (pkk_nonneg k dge1)],   
end 



-- the characteristic sequence converges to (μ {0}).to_real
lemma char_seq_converges 
	{μ: measure ℝ} [finite_measure μ]
	(μ_spec_meas: @is_spectral_measure matrix_ring ring_mat_ring _ tr_alg A μ d)
	(dge1: (d:ℝ) ≥ (1:ℝ))
	: seq_limit char_seq  (μ {0}).to_real
:= begin 
    /-the proof now relies mainly on the dominated convergence theorem
    measure_theory.tendsto_integral_of_dominated_convergence
    we collect all of its hypotheses here
    -/

    --Firstly, we need a sequence of functions (F_n) and a limit function f
    let F : ℕ → ℝ → ℝ 
        := (λ k, λ x, polynomial.eval x (pk k)),
    let f : ℝ → ℝ
        := (λ x, ite ((x:ℝ)=0) 1 0),
    -- and a bound, in this case, this is the constant function with value 1
    let bound : ℝ → ℝ 
        := (λ x, 1),

    --For technical reasons, we have to consider the measure μ', which is μ 
    -- restricted to the interval [0, d]
    let μ' :=μ.restrict (Icc 0 (d:ℝ)),

    -- The functions F n are measurable because polynomial functions are continuous, 
    -- hence in particular almost everywhere measurable
    have F_ae_measurable : ∀ n, ae_measurable (F n) μ'
        := (λ n, measurable.ae_measurable 
                        (continuous.measurable (polynomial.continuous (pk n)))),
    -- the same holds true for the function f
    have f_ae_measurable: ae_measurable f μ'
        := measurable.ae_measurable dirac_measurable,
    -- the bound function is integrable with respect to μ', because μ' is supported
    -- on a compact interval
    have bound_integrable : measure_theory.integrable bound μ',
    {
        dsimp[bound],
        exact continuous.integrable_on_compact compact_Icc (continuous_const),
    },

    -- the function bound bounds indeed F n almost everywhere
    -- (because this is wrt μ', it suffices to check boundedness on [0,d])
    have h_bound:  ∀ (n : ℕ), ∀ᵐ (a : ℝ) ∂μ', ∥F n a∥ ≤ bound a,
    {
        assume n:ℕ,
        --Note: ∀ᵐ is defined via filter.eventually P (measure.ae μ)
        have : ∀ (a:ℝ), a ∈ Icc 0 (d:ℝ) → ∥F n a∥ ≤ bound a,
        {
            intros a ainint,
            dsimp[bound],
            dsimp[F],
            simp,
            exact Fnlebound ainint,
        },
        exact allm_of_restrict (measurable_set_Icc) this,
    },

    -- The function F n converge μ'-almost everywhere to f
    have h_lim : ∀ᵐ (a : ℝ ) ∂μ',
             filter.tendsto (λ (n : ℕ), F n a) filter.at_top (nhds (f a)),
    {
        -- this is due to the fact that we have convergence on the interval [0,d]
        have : ∀ (a:ℝ), a ∈ Icc 0 (d:ℝ) → 
            filter.tendsto (λ (n : ℕ), F n a) filter.at_top (nhds (f a)),
        {
            intros a ainint,
            by_cases h: a=0,
            {
                rw h,
                dsimp[F],
                simp,
                dsimp[f],
                simp,
                exact tendsto_const_nhds,
            },
            {
                dsimp[f],
                rw if_neg h,
                dsimp [F],
                simp,
                have ge0: 1 - (↑d)⁻¹ * a ≥ 0,
                {
                    simp,
                    rw mul_comm,
                    change a / (↑ d) ≤ 1,
                    exact div_le_one_of_le ainint.2 (d_nonneg dge1), 
                },
                have lt1:  1 - (↑d)⁻¹ * a < 1,
                {
                    simp,
                    have dpos : 0 < (d:ℝ) := gt_of_ge_of_gt dge1 zero_lt_one,
                    have apos : 0 < a := lt_of_le_of_ne ainint.1 (ne.symm h),
                    rw mul_comm,
                    change a / (↑ d) > 0,
                    exact div_pos apos dpos,
                },
                exact geometric_sequence ge0 lt1,
            },
        },
        exact allm_of_restrict (measurable_set_Icc) this,
    },


    --Hence, the dominated convergence theorem yields that the integrals
    -- of the F n  tend to the integral of f 
    have convergence :
    filter.tendsto (λ (n : ℕ), ∫ (a : ℝ), (λ (n : ℕ), F n) n a ∂μ') 
                    filter.at_top (nhds (∫ (a : ℝ), f a ∂μ'))
        := measure_theory.tendsto_integral_of_dominated_convergence
            bound F_ae_measurable f_ae_measurable bound_integrable 
            h_bound h_lim,

    -- we just need to rewrite this statement to obtain the claim of the theorem
    have integral_Fn: (λ  (n : ℕ), ∫ (a : ℝ), (λ (n : ℕ), F n) n a ∂μ')
                    = char_seq,
    {
        ext (n:ℕ),

        calc ∫ (a : ℝ), (λ (n : ℕ), F n) n a ∂μ'
            = ∫ (a : ℝ),  polynomial.eval a (pk n) ∂μ'
                    : by {dsimp[F],simp,}
        ... = ∫ (a :ℝ) in Icc 0 d,  polynomial.eval a (pk n) ∂μ
                    : by simp
        ... = char_seq n
                    : μ_spec_meas (pk n),
    },

    
    -- rewrite integral of f
    have integral_f : integral μ' f = (μ {0}).to_real,
    {
        have integrable_f: integrable_on (λ (x : ℝ), 
            ite (x = 0) (1:ℝ) 0) (Icc 0 ↑d) μ := 
                    integrable_delta,
        rw int_Icc_vs_Ioc (d_nonneg dge1) μ integrable_f,
        simp,
        exact interval_integral_of_delta (d_nonneg dge1),
    },


    -- the goal is then obtained by rewriting the integrals of f and F n
    rw ← integral_Fn,
    rw ← integral_f,
    -- then, it is just the statement convergence
    exact convergence,
end 

 include matrix_ring tr_alg A d d_ge1 μ_spec_meas
 
 theorem general_RC 
 	: RC_from char_seq (μ {0}).to_real
:= begin 
    -- we unfold the definition of RC_from
    change ∃ (q : ℕ → ℝ), el_computable_from char_seq q 
                ∧ seq_limit q (μ {0}).to_real 
                ∧ ∀ (n : ℕ), q n ≥ (μ {0}).to_real,
    -- such a sequence is given the characteristic sequence itself
    use char_seq,

    have h₁ : el_computable_from char_seq char_seq
            := el_computable_from.oracle_computable _,

    have h₂ : seq_limit char_seq (μ {0}).to_real
            := char_seq_converges A μ_spec_meas d_ge1,

    have h₃ : ∀ (n : ℕ), char_seq n ≥ (μ {0}).to_real
            := char_seq_is_above A μ_spec_meas d_ge1,

    show _,
        by exact and.intro h₁ (and.intro h₂ h₃),
end 



end general_RC
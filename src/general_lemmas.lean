/-
Copyright (c) 2022 Clara Löh and Matthias Uschold. All rights reserved.
Released under Apache 2.0 license as described in the file LICENSE.txt.
Authors: Clara Löh and Matthias Uschold.
-/

/-lemmas for the general cases-/
import tactic
import .quantitative_Lueck_lemmas
import .computable_sequences

open classical 
open real 
open measure_theory
open set 

section preparation

variable (d:ℚ)

local notation `pk` k  :=  (polynomial.C 1- polynomial.C(1/(d:ℝ)) * polynomial.X)^k



-- the function of pk is integrable on an Icc integravl
lemma pkk_function_is_integrable_Icc 
  (μ: measure ℝ) 
  [locally_finite_measure μ]
  {a b:ℝ}
  (k:ℕ) (d:ℝ)
  : integrable_on (λ (x : ℝ), (1-d⁻¹*x)^k) (Icc a b) μ 
:= begin 
 have := poly_function_is_integrable_Icc μ a b (pk k),
 simp at this,
 exact this,
end

lemma pkk_nonneg (k:ℕ) {d:ℝ} (dge1: d ≥ 1)
  : ∀ (x : ℝ), x ∈ set.interval 0 d → 0 ≤  (1 - d⁻¹ * x) ^ k 
:= begin 
    intros x xinint,
    have : 0 ≤ 1 - d⁻¹ * x,
    {
      simp,
      unfold interval at xinint,
      have :  max 0 d = d  := max_eq_right (by linarith),
      rw this at xinint,
      have :  min 0 d = 0  := min_eq_left (by linarith),
      rw this at xinint,
      unfold Icc at xinint,
      have h₁: d⁻¹ * x ≤ d⁻¹*d :=  mul_le_mul_of_nonneg_left 
                      xinint.2 (le_of_lt (inv_pos.mpr (by linarith))),
      have: d≠ 0 := by linarith,
      rw (inv_mul_cancel this) at h₁,
      exact h₁,
    },
    exact pow_nonneg this k,
end

lemma dirac_measurable {a:ℝ}
: measurable (λ (x:ℝ), ite (x=a) (1:ℝ) (0:ℝ))
:= begin 
    have: measurable (set.restrict (λ (x:ℝ), ite (x=a) (1:ℝ) 0) {x : ℝ | x ≠ a}),
    {
        have : set.restrict (λ (x:ℝ), ite (x=a) (1:ℝ) 0) {x : ℝ | x ≠ a}
                = (λ x, 0),
        {
            ext,
            rw set.restrict_apply _ _,
            rw if_neg,
            exact subtype.prop x,
        },
        rw this,
        exact measurable_const,
    },
    exact measurable_of_measurable_on_compl_singleton a this,
end 

/-for a restricted measure, it suffices
to check the property on the set restricted to -/
lemma allm_of_restrict 
  {μ: measure ℝ}
  {s:set ℝ} 
  (hs: measurable_set s) 
  {p:ℝ → Prop} 
  (h: ∀x, x ∈ s → p x)
  : ∀ᵐ (x:ℝ) ∂(μ.restrict s), p x
:= begin 
  rw measure_theory.ae_iff,
  have hsubset: {x:ℝ | ¬ p x} ⊆ sᶜ,
  {
    intros x hx ins,
    simp at hx,
    exact hx (h x ins),
  },
 have le_zero: (μ.restrict s) {x:ℝ | ¬ p x} ≤ (μ.restrict s)(sᶜ)
  := measure_mono hsubset,
  have scompl_null: (μ.restrict s)(sᶜ) = 0,
  {
    rw measure_theory.measure.restrict_apply (measurable_set.compl hs),
    rw compl_inter_self s,
    exact measure_theory.measure_empty,
  },
  rw scompl_null at le_zero,
  have: ∅ ⊆  {a:ℝ | ¬ p a},
  {
    intros x h,
    exfalso,
    exact not_mem_empty x h,
  },
  have : (μ.restrict s) ∅ ≤ (μ.restrict s) {a : ℝ | ¬p a} 
    := measure_theory.measure_mono this,
  rw measure_theory.measure_empty at this,
  exact le_antisymm le_zero this,
end  



lemma Fnlebound {a d:ℝ} {n:ℕ}
  (ha: a∈ Icc 0 d) 
  : ∥1 - (↑d)⁻¹ * a∥ ^ n ≤ 1
:= begin 
    have h1: ∥1 - (↑d)⁻¹ * a∥ ≥ 0 := norm_nonneg (1 - (↑d)⁻¹ * a),
    have : ∥1 - (↑d)⁻¹ * a∥ ≤ 1,
    {
        have a_nonneg : a ≥ 0 := ha.1,
        have d_nonneg : d ≥ 0 := le_trans ha.1 ha.2,
        have: 1 - (↑d)⁻¹ * a ≥ 0,
        {
          simp,
          by_cases d=0,
          {
            rw h,
            simp,
            exact zero_le_one,
          },
          {
            have dpos: d>0 := (ne.symm h).le_iff_lt.mp d_nonneg,
            rw mul_comm,
            change  a/d ≤ 1,
            rw div_le_one dpos,
            exact ha.2,
          }
        },
        have : ∥1 - (↑d)⁻¹ * a∥ =1 - (↑d)⁻¹ * a 
          := norm_of_nonneg this,
        rw this,
        simp,
        
        have dinv_nonneg : d⁻¹ ≥ 0 := inv_nonneg.mpr d_nonneg,
        exact mul_nonneg dinv_nonneg a_nonneg,
    },
    exact pow_le_one n h1 this,
end 

lemma integrable_delta 
  {μ: measure ℝ} [finite_measure μ] {d:ℝ}
  : integrable_on (λ (x : ℝ), ite (x = 0) (1:ℝ) 0) (Icc 0 d) μ
:= begin 
  let f : ℝ → ℝ := (λ (x : ℝ), (1:ℝ)),
  have one_integrable: integrable_on  f {(0:ℝ)} μ,
  {
    rw measure_theory.integrable_on_const,
    right,
    exact measure_theory.measure_lt_top μ {0},
  },
  have hs: measurable_set {(0:ℝ)},
  {
    have: is_closed {(0:ℝ)},
        {rw ← Icc_self (0:ℝ), exact is_closed_Icc},
    exact @is_closed.measurable_set ℝ {(0:ℝ)} _ _ _  this,
  },
  
  have : integrable (set.indicator {(0:ℝ)} f)   μ 
        := @measure_theory.integrable_on.indicator ℝ ℝ _ _ _ f {0} μ one_integrable hs,
  exact measure_theory.integrable.integrable_on this,
end 



lemma interval_integral_of_delta 
  {μ: measure ℝ} [finite_measure μ] {d:ℝ} 
  (d_nonneg: d ≥ 0)
  : ∫ (x : ℝ) in 0..d, ite (x = 0) (1:ℝ) 0 ∂μ = 0
:= begin 
  rw interval_integral.integral_of_le d_nonneg,
  have hs : measurable_set (Ioc (0:ℝ) d) := measurable_set_Ioc,
  have : set.eq_on (λ (x:ℝ), ite (x = 0) (1:ℝ) 0) (λ x, (0:ℝ)) (Ioc (0:ℝ) d),
  {
    unfold eq_on,
    intros x xin,
    cases xin,
    have: x ≠ 0 := ne_of_gt xin_left,
    rw if_neg this,
  },
  rw measure_theory.set_integral_congr measurable_set_Ioc this,
  rw measure_theory.set_integral_const (0:ℝ);simp,
end 

lemma prod_nat_cast {m n : ℕ} 
  : (m:ℝ) * (n:ℝ) = (↑(m*n):ℝ)
:= begin 
  induction m; simp,
end 

open el_computable_from

lemma LC1_sequence_computable
  {t d :ℚ} {oracle: ℕ → ℝ }
  : el_computable_from oracle (λ (k : ℕ), t *(1-(k*d)⁻¹)^(k*k))
:= begin 
    have t_comp : el_computable_from oracle (λ (k : ℕ), t) := constant_computable _ t,
    have one_comp: el_computable_from oracle (λ (k : ℕ), ↑ (1:ℚ)) := constant_computable _ 1,
    have kd_comp : el_computable_from oracle (λ (k : ℕ), (↑k * d))
                  := product_computable _ (inclusion_computable _)
                      (constant_computable _ d),
    have one_over_kd_comp : el_computable_from  oracle (λ (k : ℕ), (↑k * d)⁻¹)
                  := inv_el_computable_from kd_comp,
    have first_brack_comp : el_computable_from oracle (λ (k : ℕ), ((1:ℚ) - (↑k * d)⁻¹))
          := diff_el_computable_from one_comp one_over_kd_comp,
    have kk_comp : el_computable_from oracle (λ k, (k)*(k))
                := product_computable _ (inclusion_computable _) (inclusion_computable _),
    have almost_first : el_computable_from oracle  
    (λ (k : ℕ), ((1:ℚ) - (↑k * d)⁻¹) ^ (↑k * ↑k)) 
        := pow_computable _ first_brack_comp kk_comp,
    have first : el_computable_from  oracle
    (λ (k : ℕ), t * ((1:ℚ) -  (↑k * d)⁻¹) ^ (↑k * ↑k)) := 
            product_computable _ (constant_computable _ t) almost_first,
    have : (λ (n : ℕ), ↑t * (((1:ℚ):ℝ) - ((n:ℝ) * (d:ℝ))⁻¹) ^ (↑n * ↑n) )
        = (λ (k : ℕ), ↑t * (1 - (↑k * (d:ℝ))⁻¹) ^ (k * k)),
    {
      ext (k:ℕ ),
      ring_nf,
      simp,
      left,
      have : (k:ℝ) * (k:ℝ) = (↑ (k*k) :ℝ )  := prod_nat_cast,
      rw this,
      exact rpow_nat_cast (1 - (↑k * ↑d)⁻¹) (k * k),
    },
    rw this at first,
    exact first,
end 

end preparation 



section log_lemmas

lemma log_pos_of_ge2 {k:ℕ } 
  (kge2: k ≥ 2)
  : log k > 0
:= begin 
    apply real.log_pos,
    exact nat.one_lt_cast.mpr (nat.succ_le_iff.mp kge2),
end 


lemma log_int_0_1_le_0_kinv 
  {μ: measure ℝ}
  (log_integrable: integrable log μ)
  {k:ℕ} (kge2: k≥2)
  : - ∫(x:ℝ) in 0..1, log x ∂μ  ≥ - ∫(x:ℝ) in 0..(k:ℝ)⁻¹, log x ∂μ
:= begin 
    have int_left : interval_integrable (λ x, log x) μ (0:ℝ) (k:ℝ)⁻¹
        := measure_theory.integrable.interval_integrable 
                log_integrable,
    have int_right : interval_integrable (λ x, log x) μ (k:ℝ)⁻¹ (1:ℝ)
        := measure_theory.integrable.interval_integrable 
                log_integrable,
          
    rw ← interval_integral.integral_add_adjacent_intervals
                      int_left int_right,
    simp,
    
    have : interval_integral log (↑k)⁻¹ 1 μ = - (interval_integral (λ x, - log x) (↑k)⁻¹ 1 μ),
    {
      rw  interval_integral.integral_neg,
      simp,
    },
    have neg_log_int_nonneg: ∫(x:ℝ) in (↑k)⁻¹ .. (1:ℝ), (λ x, - log x) x ∂μ  ≥ 0,
    {
      have nonneg : ∀ (x:ℝ), x ∈ set.interval (k:ℝ)⁻¹ (1:ℝ ) →
                      - log x ≥  0,
      {
        intros x xin,
        rw set.interval_of_le kinv_le1 at xin,
        cases xin,
        simp,
        exact real.log_nonpos (by linarith[kinv_pos kge2]) xin_right,
      },
      exact interval_integral.integral_nonneg kinv_le1 nonneg,
    },
    linarith,
end


lemma log_int_bounded_measure
    {μ: measure ℝ} [finite_measure μ ]
    (log_integrable: integrable log μ)
    {k:ℕ} 
  : - (∫(x:ℝ) in 0..(k:ℝ)⁻¹, log x ∂μ)
  ≥ - (log (k:ℝ)⁻¹ * (μ (Ioc 0 (k:ℝ)⁻¹)).to_real)
:= begin 

    have bounded : ∀ (x:ℝ), x∈ Ioc 0 (k:ℝ)⁻¹ → 
                                    log x ≤ log (k:ℝ)⁻¹,
    {
      intros x xin,
      cases xin,
      exact (real.log_le_log xin_left (by linarith)).mpr xin_right,
    },
    have bounded' : ∀ᵐ (x:ℝ) ∂(μ.restrict (Ioc 0 (k:ℝ)⁻¹)),
                           log x ≤ log (k:ℝ)⁻¹
        := allm_of_restrict (measurable_set_Ioc) bounded,
    have h₁: ∫(x:ℝ) in 0..(k:ℝ)⁻¹, log x ∂μ
                     ≤ ∫ (x:ℝ) in (Ioc (0:ℝ) (k:ℝ)⁻¹),
                            log (k:ℝ)⁻¹ ∂μ,
    {
      rw interval_integral.integral_of_le kinv_nonneg,
      have log_int : integrable log (μ.restrict (Ioc (0:ℝ) (k:ℝ)⁻¹))
                  := measure_theory.integrable.integrable_on log_integrable,
      have const_int : integrable (λ x, log (k:ℝ)⁻¹) (μ.restrict (Ioc (0:ℝ) (k:ℝ)⁻¹)),
      {
         change integrable_on (λ x, log (k:ℝ)⁻¹) (Ioc (0:ℝ) (k:ℝ)⁻¹) μ,
         rw measure_theory.integrable_on_const,
         right,
         exact measure_theory.measure_lt_top μ  (Ioc (0:ℝ) (k:ℝ)⁻¹),
      },
      exact measure_theory.integral_mono_ae log_int const_int bounded',
    },
    have h₂ : ∫ (x:ℝ) in (Ioc (0:ℝ) (k:ℝ)⁻¹),
                            log (k:ℝ)⁻¹ ∂μ = 
              (log (k:ℝ)⁻¹ * (μ (Ioc 0 (k:ℝ)⁻¹)).to_real),
    {
      rw measure_theory.integral_const (log (k:ℝ)⁻¹),
      simp,
      rw mul_comm,
    },
    rw h₂ at h₁,
    exact neg_le_neg h₁,
end




end log_lemmas

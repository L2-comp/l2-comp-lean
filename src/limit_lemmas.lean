/-
Copyright (c) 2022 Clara Löh and Matthias Uschold. All rights reserved.
Released under Apache 2.0 license as described in the file LICENSE.txt.
Authors: Clara Löh and Matthias Uschold.
-/

/- this file provides some lemmas on limits-/
import tactic 
import topology.basic
import data.real.basic
import topology.algebra.polynomial
import analysis.special_functions.exp_log

open classical
open real 
open set


/-we define seq_limit to simplify notation
this means that lim_(n→∞) a n = l-/
--the filter.at_top is more common in the mathlib
-- see e.g. analysis.specific_limits

def seq_limit (a:ℕ → ℝ) (l:ℝ)   
  : Prop
:= filter.tendsto a filter.at_top (nhds l)


namespace inequalities
-- some general inequalities that are often needed in the
-- proof of the quantitative Lueck approximation theorem 
-- and general_RC, general_bound_LC, etc.

lemma kinv_nonneg {k:ℕ}
  : (k:ℝ)⁻¹ ≥ 0
:= inv_nonneg.mpr (nat.cast_nonneg k)

lemma kinv_pos {k:ℕ} 
  (hk: k≥ 2) 
  : (k:ℝ)⁻¹ > 0 
:= begin 
  have : k > 1 := nat.succ_le_iff.mp hk,
  have : k > 0 := lt_trans zero_lt_one this,
  have : (k:ℝ) > 0 := nat.cast_pos.mpr this,
  exact inv_pos.mpr this,
end 

lemma kinv_le1 {k:ℕ} 
  : (k:ℝ)⁻¹ ≤ 1
:= begin 
  by_cases k=0,
  rw h; simp; exact zero_le_one,
  have: (k:ℝ) ≥ 1 := nat.one_le_cast.mpr 
                (nat.succ_le_iff.mpr (zero_lt_iff.mpr h)),
  exact inv_le_one this,
end

lemma kinv_led {k:ℕ} {d:ℝ} 
  (d_ge1 : d ≥ 1) 
  : (k:ℝ)⁻¹ ≤ d 
:= le_trans kinv_le1 d_ge1

lemma d_nonneg {d:ℝ} 
  (d_ge1 : d ≥ 1) 
  : d ≥ 0 
:= le_trans zero_le_one d_ge1



lemma dinv_nonneg {d:ℝ} 
  (d_ge1 : d ≥ 1) 
  : d⁻¹ ≥ 0
:= inv_nonneg.mpr (d_nonneg d_ge1)



lemma kd_ge1'  {k:ℕ} 
  (k_ge1: k≥ 1) 
  {d:ℝ} 
  (d_ge1 : d ≥ 1) 
  : (k:ℝ)*d ≥ 1
:= 
begin 
   have: (k:ℝ) ≥ 1 := nat.one_le_cast.mpr k_ge1,
   exact one_le_mul_of_one_le_of_one_le this d_ge1,
end 

lemma kd_ge1  
  {k:ℕ} 
  (k_ge2: k≥ 2) 
  {d:ℝ} 
  (d_ge1 : d ≥ 1) 
  : (k:ℝ)*d ≥ 1
:= kd_ge1' (nat.le_of_succ_le k_ge2) d_ge1

lemma kd_inv_le1 {k:ℕ} {d:ℝ} 
  (d_ge1 : d ≥ 1) 
  : ((k:ℝ)*d)⁻¹ ≤ 1
:= begin 
  by_cases h: k=0,
  {
    rw h,
    simp,
    exact zero_le_one,
  },
  {
    have : k≥ 1:= nat.succ_le_iff.mpr (zero_lt_iff.mpr h),
    exact inv_le_one (kd_ge1' this d_ge1),
  },
end 

end inequalities

section lemmas

notation `|` x `|` := abs x


-- the seq_limit is equal to the classical definition
lemma seq_limit_classical {a:ℕ → ℝ} {l:ℝ} 
  : seq_limit a l ↔ ∀ ε >0, ∃ N, ∀n≥ N, |a n - l| < ε
:=
begin  
  unfold seq_limit,
  rw  metric.tendsto_at_top,
  split,
  {
    intros h ε εpos,
    cases h ε εpos with N,
    use N,
    intros n ngeN,
    rw ← real.dist_eq (a n) l,
    exact h_1 n ngeN,
  },
  {
    intros h ε εpos,
    cases h ε εpos with N,
    use N,
    intros n ngeN,
    rw real.dist_eq (a n) l,
    exact h_1 n ngeN,
  },
end 

lemma const_converges {t:ℝ} 
  : seq_limit (λ (k:ℕ), t) t 
:= tendsto_const_nhds

/- If a converges to l, b to m, then (a+b) converges to (l+m)-/
lemma sum_seq_limit {a b:ℕ → ℝ} {l m :ℝ }
  (ha: seq_limit a l) 
  (hb: seq_limit b m)
  : seq_limit (a+b) (l+m)
:= begin 
  rw seq_limit_classical at *,
  intros ε εpos,
  specialize ha (ε/2) (by linarith),
  cases ha with N₁,
  specialize hb (ε/2) (by linarith),
  cases hb with N₂,
  use max N₁ N₂,
  intros n ngemax,
  specialize ha_h n (le_of_max_le_left ngemax),
  specialize hb_h n (le_of_max_le_right ngemax),
  simp,
  calc |a n + b n - (l+m)| = |(a n - l) + (b n - m)| : by congr'; ring
  ... ≤ |(a n - l)| + |(b n - m)| : abs_add (a n - l) (b n - m)
  ... < (ε/2) + |(b n - m)| : by linarith [ha_h]
  ... < (ε/2) + (ε/2) : by linarith [hb_h]
  ... = ε : by ring,
end 

lemma diff_seq_limit {a b:ℕ → ℝ} {l m :ℝ }
  (ha: seq_limit a l) 
  (hb: seq_limit b m)
  : seq_limit (a- b) (l-m)
:= sum_seq_limit ha (filter.tendsto.neg hb)


/-the squeezing lemma-/
lemma squeezing {u v w: ℕ → ℝ } {l:ℝ } 
  (hu : seq_limit u l) 
  (hw : seq_limit w l)
  (h : ∀ n, u n ≤ v n)
  (h' : ∀ n, v n ≤ w n) 
  : seq_limit v l 
:= begin
  rw seq_limit_classical at *,
  intros ε ε_pos,
  cases hu ε ε_pos with N hN,
  cases hw ε ε_pos with N' hN',
  use max N N',
  intros n hn,
  specialize hN n (le_of_max_le_left hn),
  specialize hN' n (le_of_max_le_right hn),
  specialize h n,
  specialize h' n,
  rw abs_lt at *,
  split,
  calc -ε < u n - l : by linarith
      ... ≤ v n - l : by linarith,
  calc v n - l ≤ w n - l : by linarith
      ... < ε : by linarith,
end

/-variant: inequality only starting from M-/
lemma squeezing' {u v w: ℕ → ℝ } {l:ℝ } 
  (hu : seq_limit u l) 
  (hw : seq_limit w l)
  {M:ℕ}
  (h : ∀ n ≥ M, u n ≤ v n)
  (h' : ∀ n≥ M, v n ≤ w n) 
  : seq_limit v l 
:= begin
  rw seq_limit_classical at *,
  intros ε ε_pos,
  cases hu ε ε_pos with N hN,
  cases hw ε ε_pos with N' hN',
  use max M (max N N'),
  intros n hn,
  have hn' : n ≥ max N N' := le_of_max_le_right hn,
  specialize hN n (le_of_max_le_left hn'),
  specialize hN' n (le_of_max_le_right hn'),
  have ngeM : n≥ M := le_of_max_le_left hn,
  specialize h n ngeM,
  specialize h' n ngeM,
  rw abs_lt at *,
  split,
  calc -ε < u n - l : by linarith
      ... ≤ v n - l : by linarith,
  calc v n - l ≤ w n - l : by linarith
      ... < ε : by linarith,
end

/-sequences that eventually coincide, converge to 
the same limit-/
lemma eventually_coincide_converges {a b:ℕ → ℝ } {N:ℕ} {l:ℝ}
  (heq: ∀n≥ N, a n = b n) 
  (ha: seq_limit a l)
  : seq_limit b l 
:= begin
  have h' : ∀ n≥ N, a n ≥ b n := 
        (λ n ngeN, (le_of_eq (heq n ngeN).symm)), 
  have h : ∀ n≥ N, a n ≤  b n := 
        (λ n ngeN, (le_of_eq (heq n ngeN))),
  exact squeezing' ha ha h h',
end 




lemma akk_converges {a:ℕ →  ℝ} {l:ℝ} 
  (ha: seq_limit a l)
  : seq_limit (λ k, a (k*k)) l 
:= begin 
  rw seq_limit_classical at *,
  intros ε εpos,
  cases ha ε εpos with N ha,
  use N,
  intros n ngeN,
  have square_ge_n : n*n ≥ n := nat.le_mul_self n,
  have : n*n ≥ N := le_trans ngeN square_ge_n,
  exact ha (n*n) this,
end 

lemma mul_const_converges {t l:ℝ} {a:ℕ → ℝ} 
  (ha: seq_limit a l) 
  : seq_limit (λ k, t* a k) (t*l)
:= begin
  by_cases ht: t=0,
  {
    rw ht,
    simp,
    exact const_converges,
  },
  rw seq_limit_classical at *, 
  intros ε εpos,
  have abstpos: |t| > 0 := abs_pos.mpr ht,
  have : ε / |t| >0 := div_pos εpos abstpos,
  specialize ha (ε / |t|) this,
  cases ha with N ha,
  use N,
  intros n ngeN,
  specialize ha n ngeN,
  have : t*(a n) - t*l = t* ((a n)-l) := by ring,
  rw this,
  rw abs_mul,
  exact (lt_div_iff' abstpos).mp ha,
end 


end lemmas 


section examples 




lemma bernoulli_ineq {x:ℝ} {k:ℕ}
  (x_geneg1: x ≥ -1) 
  : (1+x)^k ≥ 1+k*x 
:= begin 
  induction k,
  {
    simp,
  },
  {
    simp,
    rw pow_succ' (1 + x) k_n,
    rw add_mul ↑k_n 1 x, 
    simp,
    have : 1+x ≥ 0 := by linarith,
    have:  (1+x)^k_n * (1+x) ≥ (1+ k_n*x) *(1+x) 
      := mul_mono_nonneg this k_ih,
    have h₁: (1+ (k_n:ℝ)*x) *(1+x)
      ≥ 1+(k_n*x +x),
    {
      simp,
      rw  mul_add (1 + ↑k_n * x) 1 x,
      rw ← add_assoc,
      simp,
      rw add_mul 1 (↑k_n*x) x,
      simp,
      have h₁: (k_n:ℝ) ≥ 0 := nat.cast_nonneg k_n,
      have h₂: x*x ≥ 0 := mul_self_nonneg x,
      rw mul_assoc,
      exact mul_nonneg h₁ h₂,
    },
    linarith,
  }
end 

open inequalities

lemma geometric_sequence {x:ℝ} 
  (x_nonneg: x≥ 0)
  (x_ltone: x < 1)
  : seq_limit (λ k, x^k) 0
:= begin 
  rw seq_limit_classical,
  by_cases x=0,
  {
    rw h,
    intros ε εpos,
    use 1,
    intros n nge1,
    have: (0:ℝ)^n= 0:= zero_pow' n (ne_of_gt (nat.succ_le_iff.mp nge1)),
    rw this,
    simp,
    exact εpos,
  },
  have xpos : x > 0 := (ne.symm h).le_iff_lt.mp x_nonneg,
  let M:ℝ := x⁻¹ -1,
  have Mpos : M > 0,
  {
    simp,
    by_contradiction h₁,
    simp at h₁,
    have : x⁻¹*x ≤ 1*x := mul_mono_nonneg x_nonneg h₁,
    have h': x⁻¹*x = 1 := inv_mul_cancel h,
    rw h' at this,
    simp at this,
    by linarith,
  },
  
  intros ε εpos,
  have εMinv_pos: (ε*M)⁻¹ > 0 := inv_pos.mpr (mul_pos εpos Mpos),
  have : ∃(N:ℕ), (N:ℝ) >  (ε*M)⁻¹
        := exists_nat_gt (ε*M)⁻¹,
  cases this with N hn,
  use N,
  intros n ngeN,
  simp,
  rw abs_of_pos xpos,
  have h₁: x = (1+M)⁻¹,{simp,},
  have h₂: ((1+M)⁻¹)^n = ((1+M)^n)⁻¹:= inv_pow' (1 + M) n,
  have h₃: x^n = ((1+M)^n)⁻¹ := tactic.ring_exp.pow_pf_c_c h₁ rfl h₂,
  have h₄:(1+M)^n ≥ 1 +n*M := bernoulli_ineq (by linarith),
  have h₅: (1:ℝ)+n*M ≥ n*M,{simp,exact zero_le_one,},
  have : M≥ 0 := by linarith,
  have ngeNR: (n:ℝ) ≥ N := nat.cast_le.mpr ngeN,
  have h₆: (n:ℝ)*M ≥ N*M := mul_mono_nonneg this ngeNR,
  have h₇: (1+M)^n ≥ N*M := le_trans h₆ (le_trans h₅ h₄),
  have : (N:ℝ)*M > 0,
  {
    have: (N:ℝ) > 0 := lt_trans εMinv_pos hn,
    exact mul_pos this Mpos,
  },
  have h₈: ((1+M)^n)⁻¹ ≤ (N*M)⁻¹:= inv_le_inv_of_le this h₇,
  rw ← h₃ at h₈,
  have h₉: ((N:ℝ)*M)⁻¹ < ε,
  {
    rw inv_lt this εpos,
    have h':  (N:ℝ)*M > (ε*M)⁻¹ *M := (mul_lt_mul_right Mpos).mpr hn,
    simp at h',
    have:  (ε*M)⁻¹ = M⁻¹*ε⁻¹ := mul_inv_rev' ε M,
    rw this at h',
    rw mul_comm at h',
    rw ← mul_assoc at h',
    rw mul_inv_cancel (ne_of_gt Mpos) at h',
    simp at h',
    exact h',
  },
  exact gt_of_gt_of_ge h₉ h₈,
end 

open nat


lemma ε₁_to_zero'_ineq {d:ℝ} {k:ℕ} 
  (dge1: d ≥ 1)  
  (kge1: k ≥ 1)
  : (1 - 1 / (↑k * d)) ^ k ≤ d/(d+1)
:= begin 
  have dpos : d > 0 := by linarith,
  have dpluspos: d+1 > 0 := by linarith,
  by_cases h: (k:ℝ)*d = 1,
  {
    rw h,
    simp,
    have : k> 0 := nat.succ_le_iff.mp kge1,
    rw zero_pow this,
    have : d/(d+1) > 0 := div_pos dpos dpluspos,
    exact le_of_lt this,
  },
  have kd_ge_one: (k:ℝ) *d > 1,
  {
    have : (k:ℝ) *d ≥ 1 := kd_ge1' kge1 dge1,
    exact (ne.symm h).le_iff_lt.mp this,
  },
  have h₁: 1 - 1 / (↑k * d) = (1 + ((k:ℝ)*d-1)⁻¹)⁻¹,
  {
    simp,
    have kd_neq_zero: ((k:ℝ) * d) ≠ 0 := by linarith,
    have kdone_neq_zero: (k:ℝ)*d -1 ≠ 0 := by linarith,
    have : (1 + (↑k * d - 1)⁻¹) ≠ 0,
    {
      intro h',
      rw ← (mul_left_inj' kdone_neq_zero) at h',
      rw zero_mul at h',
      rw add_mul at h',
      rw inv_mul_cancel kdone_neq_zero at h',
      rw one_mul at h',
      linarith,
    },
    rw ← (mul_left_inj' this),
    rw inv_mul_cancel this,
    rw sub_mul,
    rw mul_add,
    simp,
    rw mul_add,
    simp,
    rw ← add_sub,
    simp,
    rw ← (mul_left_inj' kdone_neq_zero),
    rw zero_mul,
    rw sub_mul,
    rw inv_mul_cancel kdone_neq_zero,
    rw add_mul,
    rw mul_assoc,
    rw inv_mul_cancel kdone_neq_zero,
    rw ← mul_add,
    simp,
    rw inv_mul_cancel kd_neq_zero,
    simp,
  },
  have h₂ : (1 - 1 / (↑k * d))^k = ((1 + ((k:ℝ)*d-1)⁻¹)^k)⁻¹,
  {
    rw h₁,
    exact inv_pow' (1 + (↑k * d - 1)⁻¹) k,
  },
  have kdone_ge_neg1 : ((k:ℝ)*d-1)⁻¹ ≥ -1,
  {
    have : (k:ℝ)*d-1 > 0 := by linarith, 
    have : ((k:ℝ)*d-1)⁻¹ > 0 := inv_pos.mpr this,
    linarith,
  },
  have bernoulli : (1 + ((k:ℝ)*d-1)⁻¹)^k ≥ 1 + k* ((k:ℝ)*d-1)⁻¹
        := bernoulli_ineq kdone_ge_neg1,
  have h₃ : 1 + (k:ℝ)* ((k:ℝ)*d-1)⁻¹ ≥ 1 + d⁻¹,
  {
    simp,
    rw ← one_mul (d:ℝ)⁻¹,
    change 1/d ≤ (k:ℝ) / ((k:ℝ)*d-1),
    have kdnegone_pos : (k:ℝ)*d-1 > 0 := by linarith,
    rw div_le_div_iff dpos (kdnegone_pos),
    simp,
    exact zero_le_one,
  },
  have : d⁻¹ > 0 := inv_pos.mpr dpos,
  have one_plus_dinv_pos: 1+d⁻¹ > 0 := by linarith,
  have one_plus_d_inv_inv : (1+d⁻¹)⁻¹ = d/(d+1),
  {
    rw ← one_mul (1+d⁻¹)⁻¹,
    change 1/(1+d⁻¹) = d/(d+1),
    rw div_eq_div_iff (ne_of_gt one_plus_dinv_pos) (ne_of_gt dpluspos),
    simp,
    rw mul_add,
    rw mul_inv_cancel (ne_of_gt dpos),
    simp,
  },
  have ineq: (1 + ((k:ℝ)*d-1)⁻¹)^k ≥ 1 + d⁻¹ 
          := le_trans h₃ bernoulli,
  
  have ineq_inv: ((1 + ((k:ℝ)*d-1)⁻¹)^k)⁻¹ ≤  (1 + d⁻¹)⁻¹
    := inv_le_inv_of_le one_plus_dinv_pos ineq,
  calc (1 - 1 / (↑k * d)) ^ k = ((1 + ((k:ℝ)*d-1)⁻¹)^k)⁻¹ : h₂
                          ... ≤ (1 + d⁻¹)⁻¹   : ineq_inv
                          ... = d/(d+1) : one_plus_d_inv_inv,
end 


lemma ε₁_to_zero' {d :ℝ} 
  (dge1: d ≥ 1)
  : seq_limit (λ (k : ℕ), (1 - 1 / (↑k * d)) ^ (k * k)) 0
:= begin 
  let a : ℕ → ℝ := (λ (k : ℕ), (1 - 1 / (↑k * d)) ^ (k * k)),
  let zero: ℕ → ℝ := (λ k, 0),
  let geometric : ℕ → ℝ := (λ k, (d/(d+1))^k),
  have apos : ∀ k≥1, a k ≥ zero k,
  {
    intros k kge1,
    dsimp[zero],
    dsimp[a],
    have : 1 - 1 / (↑k * d) ≥ 0,
    {
      simp,
      exact kd_inv_le1 dge1,
    },
    exact pow_nonneg this (k * k),
  },
  have zero_conv : seq_limit zero 0 := const_converges,
  have a_le_geometric : ∀ k ≥ 1, a k ≤ geometric k,
  {
    intros k kge1,
    dsimp[a],
    dsimp[geometric],
    have : (1 - 1 / (↑k * d)) ^ (k * k) = ((1 - 1 / (↑k * d)) ^ k)^k
          := pow_mul (1 - 1 / (↑k * d)) k k,
    rw this,
    have powk_nonneg : (1 - 1 / (↑k * d)) ^ k ≥ 0,
    {
      have : 1 - 1 / (↑k * d) ≥ 0,
      {
        simp,
        exact kd_inv_le1 dge1,
      },
      exact pow_nonneg this k,
    },
    have : (1 - 1 / (↑k * d)) ^ k ≤ d/(d+1)
              := ε₁_to_zero'_ineq dge1 kge1,
    exact pow_le_pow_of_le_left powk_nonneg this k,
  },
  have dnonneg: d≥ 0 := by linarith,
  have : d+1 ≥ 0 := by linarith,
  have dplusone_pos: d+1 >0 := by linarith,
  have quot_nonneg : d/(d+1) ≥ 0 := div_nonneg dnonneg this,
  have quot_lt_one: d/(d+1) < 1,
  {
    rw div_lt_one dplusone_pos,
    simp,
    exact zero_lt_one,
  },
  have geometric_conv : seq_limit geometric 0 := 
      geometric_sequence quot_nonneg quot_lt_one,
  exact squeezing' zero_conv geometric_conv apos a_le_geometric,
end 


lemma ε₁_to_zero {t d :ℚ} 
  (dge1: (d:ℝ) ≥ 1)
  : seq_limit (λ (k : ℕ), t * (1 - 1 / (↑k * d)) ^ (k * k)) 0
:= begin 
  have := @mul_const_converges t (0:ℝ) _ (ε₁_to_zero' dge1),
  simp at this,
  simp,
  exact this,
end 

lemma log_inv_to_zero 
  : seq_limit (λ (k:ℕ), (log k)⁻¹) (0:ℝ)
:= begin 
  rw seq_limit_classical,
  intros ε εpos,
  have : ∃(N:ℕ), (N:ℝ) > exp (1/ε) 
          := exists_nat_gt _,
  cases this with N hN,
  have Npos : (N:ℝ) > 0 := lt_trans (1 / ε).exp_pos hN,
  let N' := max N 2,
  use N',
  intros n ngemax,
  have nge2 : n ≥ 2 :=  le_of_max_le_right ngemax,
  have ngeN : n ≥ N := le_of_max_le_left ngemax,
  have ngeN_real: (n:ℝ) ≥ (N:ℝ) := nat.cast_le.mpr ngeN,
  have ngt1: n>1 := nat.succ_le_iff.mp nge2, 
  have ngt1': (n:ℝ) > (1:ℝ) := nat.one_lt_cast.mpr ngt1,
  have npos : (n:ℝ) > 0 := lt_trans zero_lt_one ngt1',
  simp,
  have logpos : log n > 0 := real.log_pos ngt1',
  have :(log n)⁻¹ > 0 := inv_pos.mpr logpos,
  rw abs_of_pos this,
  rw inv_eq_one_div (log ↑n),
  rw one_div_lt logpos εpos,
  have h₁: log n ≥ log N := (real.log_le_log Npos npos).mpr ngeN_real,
  have h₂ : log N > log (exp(1/ε)) := real.log_lt_log (1 / ε).exp_pos hN,
  rw real.log_exp (1/ε) at h₂,
  exact gt_of_ge_of_gt h₁ h₂,
end

lemma mul_log_inv_to_zero {t:ℝ} 
  : seq_limit (λ (k:ℕ), t*(log k)⁻¹) (0:ℝ)
:= begin 
  have := @mul_const_converges t (0:ℝ) _ log_inv_to_zero,
  simp at this,
  exact this,
end


end examples 



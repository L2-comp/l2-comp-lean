/-
Copyright (c) 2022 Clara Löh and Matthias Uschold. All rights reserved.
Released under Apache 2.0 license as described in the file LICENSE.txt.
Authors: Clara Löh and Matthias Uschold.
-/

/-This file provides some 
technical lemmas needed in quantitative_Lueck.lean-/


import .spectral_measure
import data.polynomial.degree.definitions

open tactic
open classical
open traced_algebra 
open measure_theory
open set
open real
open interval_integral



section inequalities
-- some general inequalities that are often needed in the
-- proof of the quantitative Lueck approximation theorem

lemma kinv_nonneg {k:ℕ}
  : (k:ℝ)⁻¹ ≥ 0
:= inv_nonneg.mpr (nat.cast_nonneg k)

lemma kinv_pos {k:ℕ} 
  (hk: k≥ 2) 
  : (k:ℝ)⁻¹ > 0 
:= begin 
  have : k > 1 := nat.succ_le_iff.mp hk,
  have : k > 0 := lt_trans zero_lt_one this,
  have : (k:ℝ) > 0 := nat.cast_pos.mpr this,
  exact inv_pos.mpr this,
end 

lemma kinv_le1 {k:ℕ} 
  : (k:ℝ)⁻¹ ≤ 1
:=  begin 
  by_cases k=0,
  rw h; simp; exact zero_le_one,
  have: (k:ℝ) ≥ 1 := nat.one_le_cast.mpr 
                (nat.succ_le_iff.mpr (zero_lt_iff.mpr h)),
  exact inv_le_one this,
end

lemma kinv_led {k:ℕ} {d:ℝ} 
  (d_ge1 : d ≥ 1) 
  : (k:ℝ)⁻¹ ≤ d 
:= le_trans kinv_le1 d_ge1

lemma d_nonneg {d:ℝ} 
  (d_ge1 : d ≥ 1) 
  : d ≥ 0 
:= le_trans zero_le_one d_ge1



lemma dinv_nonneg {d:ℝ} 
  (d_ge1 : d ≥ 1) 
  : d⁻¹ ≥ 0
:= inv_nonneg.mpr (d_nonneg d_ge1)



lemma kd_ge1'  {k:ℕ} {d:ℝ}
  (k_ge1: k≥ 1) 
  (d_ge1 : d ≥ 1) 
  : (k:ℝ)*d ≥ 1
:= begin 
   have: (k:ℝ) ≥ 1 := nat.one_le_cast.mpr k_ge1,
   exact one_le_mul_of_one_le_of_one_le this d_ge1,
end 

lemma kd_ge1  {k:ℕ} 
  (k_ge2: k≥ 2) {d:ℝ} 
  (d_ge1 : d ≥ 1) 
  : (k:ℝ)*d ≥ 1
:= kd_ge1' (nat.le_of_succ_le k_ge2) d_ge1

lemma kd_inv_le1 {k:ℕ} {d:ℝ} 
  (d_ge1 : d ≥ 1) 
  : ((k:ℝ)*d)⁻¹ ≤ 1
:= begin 
  by_cases h: k=0,
  {
    rw h,
    simp,
    exact zero_le_one,
  },
  {
    have : k≥ 1:= nat.succ_le_iff.mpr (zero_lt_iff.mpr h),
    exact inv_le_one (kd_ge1' this d_ge1),
  },
end 

lemma one_kd_inv_nonneg {k:ℕ} {d:ℝ} 
  (d_ge1 : d ≥ 1) 
  : 1 - ((k:ℝ) * d)⁻¹ ≥ 0
:= begin 
    simp,
    exact kd_inv_le1 d_ge1,
end

--this specific polynomial achieves its heighest value on the interval [1/k, d]
-- at 1/k
lemma poly_in_right_interval {k:ℕ}  {d:ℝ} 
  (dge1: d ≥ 1)
  : ∀ (x:ℝ), x ∈ set.interval (k:ℝ)⁻¹ d →  
            (1 - d⁻¹ * x) ^ (k * k) ≤  (1 - (↑k * d)⁻¹) ^ (k * k)
:= begin
  intros x xinint,
        have pos : 1 - d⁻¹ * x ≥ 0,
        {
          simp,
          have : d⁻¹ > 0 := inv_pos.mpr (by linarith),
          have dinv: d⁻¹ * d = 1 := 
              inv_mul_cancel (by linarith),
          have: x ≤ d,
          {
            unfold interval at xinint,
            unfold Icc at xinint,
            have : max (k:ℝ)⁻¹ d = d := max_eq_right (kinv_led dge1),
            rw this at xinint,
            exact xinint.2,
          },
          have : d⁻¹ * x ≤ d⁻¹ * d := (mul_le_mul_left (by linarith)).mpr this,
          rw dinv at this,
          exact this,
        },
        have : 1 - d⁻¹ * x ≤ 1 - (↑k * d)⁻¹,
        {
          simp,
          rw mul_inv',
          rw mul_comm (↑k)⁻¹ d⁻¹,
          have kneglex: (k:ℝ)⁻¹ ≤ x,
          {
            rw interval_of_le (kinv_led dge1) at xinint,
            exact xinint.1,
          },
          exact mul_le_mul_of_nonneg_left kneglex (dinv_nonneg dge1),
        },
        exact pow_le_pow_of_le_left pos this (k * k),
end 

end inequalities


section integrals
-- the following fact on Icc vs Ioc integration
lemma int_Icc_vs_Ioc {a b:ℝ} 
  (hab: a≤ b)
  (μ: measure ℝ) [locally_finite_measure μ]
  {f:ℝ → ℝ}
  (fint: measure_theory.integrable_on f (Icc a b) μ)
  : ∫(x:ℝ) in Icc a b, f x ∂μ = (f a)* (μ {a}).to_real + ∫ x in a..b, f x ∂μ
:= begin
  rw integral_of_le hab,
  have Icc_aa: Icc a a = {a} := Icc_self a,
  have union: {a} ∪ Ioc a b = Icc a b,
  {
    ext,
    split,
    {
      intro h,
      cases h,
      {
        have : x=a :=mem_singleton_iff.mp h,
        unfold Icc,
        split,
        exact le_of_eq this.symm,
        rw this,
        exact hab,
      },
      {
        exact Ioc_subset_Icc_self h,
      },
    },
    {
      by_cases x=a,
      {
        intro,
        left,
        exact mem_singleton_iff.mpr h,
      },
      {
        intro inIcc,
        right,
        cases inIcc,
        split,
        exact (ne.symm h).le_iff_lt.mp inIcc_left,
        exact inIcc_right,
      },
    },
  },
  rw ← union,
  have disj: disjoint {a} (Ioc a b),
  {
    intros x xinun,
    exfalso,
    cases xinun,
    cases xinun_right,
    have : x=a := mem_singleton_iff.mp xinun_left,
    linarith,
  },  
  have meas_a: measurable_set {a},
  {
    rw ← Icc_aa,
    exact measurable_set_Icc,
  },
  have meas_ab: measurable_set (Ioc a b) := measurable_set_Ioc,
  have fint_a :  measure_theory.integrable_on f {a} μ,
  {
    rw ← union at fint,
    exact measure_theory.integrable_on.left_of_union fint,
  },
  have fint_ab :  measure_theory.integrable_on f (Ioc a b) μ,
  {
    rw ← union at fint,
    exact measure_theory.integrable_on.right_of_union fint,
  },
  rw measure_theory.integral_union disj meas_a meas_ab fint_a fint_ab,
  simp,
  rw mul_comm,
  let g: ℝ → ℝ := (λ x, f a),
  have f_eq_g : eq_on f g {a},
  {
    unfold eq_on,
    intros x xin,
    have : x=a := mem_singleton_iff.mp xin,
    rw this,
  },
  rw measure_theory.set_integral_congr meas_a f_eq_g,
  exact measure_theory.set_integral_const (f a),
end


-- integration of functions that are at most one gives at
-- most the measure
lemma int_function_btween_1 (k : ℕ) (d:ℝ) 
  (dge1:d ≥ 1) 
  (μ₁ : measure ℝ) [locally_finite_measure μ₁]
  {f: ℝ → ℝ}
  (fint: interval_integrable f μ₁ (0:ℝ) (k:ℝ)⁻¹)
  (fbound: ∀ (x:ℝ ), x ∈ interval (0:ℝ) d → f x ≤ (1:ℝ))
  : ∫ (x : ℝ) in 0..(↑k)⁻¹, f x ∂μ₁≤ (μ₁ (Ioc 0 (k:ℝ)⁻¹)).to_real
:= begin
  have ineq1: ∫ (x : ℝ) in 0..(↑k)⁻¹, f x ∂μ₁ ≤ ∫ (x : ℝ) in 0..(↑k)⁻¹, 1 ∂μ₁,
  {
    let g : ℝ → ℝ := (λ x, 1),
    have gint: interval_integrable g μ₁ (0:ℝ) (k:ℝ)⁻¹
        :=continuous.interval_integrable continuous_const (0:ℝ) (k:ℝ)⁻¹,
    have fbound': ∀ (x:ℝ ), x ∈ interval (0:ℝ) (k:ℝ)⁻¹ → f x ≤ (1:ℝ),
    {
      intros x xinint,
      specialize fbound x,
      have knegled: (k:ℝ)⁻¹ ≤ d := le_trans kinv_le1 dge1,
      have : x ∈ interval (0:ℝ) d,
      {
        unfold interval,
        unfold Icc,
        simp,
        unfold interval at xinint,
        unfold Icc at xinint,
        simp at xinint,
        cases xinint,
        split,
        {
          left,
          exact xinint_left,
        },
        {
          right,
          exact le_trans xinint_right knegled,
        },
      },
      exact fbound this,
    },
    exact interval_integral.integral_mono_on fint gint kinv_nonneg fbound',
  },
  have: ∫ (x : ℝ) in (0:ℝ)..(k:ℝ)⁻¹, (1:ℝ) ∂μ₁ = (μ₁ (Ioc 0 (k:ℝ)⁻¹)).to_real,
  {
    rw interval_integral.integral_const',
    have: Ioc (k:ℝ)⁻¹ 0 = ∅ := set.Ioc_eq_empty kinv_nonneg,
    rw this,
    rw measure_theory.measure_empty,
    simp,
  },
  rw this at ineq1,
  exact ineq1,
end


end integrals

section integrability
-- some facts on integrability


--a polynomial function is integrable
lemma poly_function_is_integrable_on 
  (μ: measure ℝ) [locally_finite_measure μ]
  (a b:ℝ)
  (p:polynomial ℝ)
  : interval_integrable (λ (x : ℝ), polynomial.eval x p) μ a b
:= begin 
  by_cases a_le_b: a ≤ b,
  exact continuous_on.interval_integrable_of_Icc 
       a_le_b (polynomial.continuous_on p),
  exact interval_integrable.symm (continuous_on.interval_integrable_of_Icc 
       (by linarith) (polynomial.continuous_on p)),
end


--and specifically for pk's function
lemma pk_function_is_integrable_on 
  (μ: measure ℝ) [locally_finite_measure μ]
  (a b:ℝ)
  (k:ℕ) 
  (d:ℝ)
  : interval_integrable (λ (x : ℝ), (1-d⁻¹*x)^(k*k)) μ a b
:= begin
 let pk := (1- polynomial.C (1/d) * polynomial.X)^(k*k),
 have := poly_function_is_integrable_on μ a b pk,
 simp at this,
 exact this,
end


--and the Icc version
lemma poly_function_is_integrable_Icc 
  (μ: measure ℝ) [locally_finite_measure μ]
  (a b:ℝ)   
  (p:polynomial ℝ)
  : integrable_on (λ (x : ℝ), polynomial.eval x p) (Icc a b) μ 
:=  continuous.integrable_on_compact compact_Icc (polynomial.continuous p)


-- and again specifically for pk's function 
lemma pk_function_is_integrable_Icc 
  (μ: measure ℝ) [locally_finite_measure μ]
  (a b:ℝ)
  (k:ℕ) 
  (d:ℝ)
  : integrable_on (λ (x : ℝ), (1-d⁻¹*x)^(k*k)) (Icc a b) μ 
:= begin 
  let pk := (1- polynomial.C (1/d) * polynomial.X)^(k*k),
 have := poly_function_is_integrable_Icc μ a b pk,
 simp at this,
 exact this,
end


end integrability



section pk_ineq
-- some inequalities on the Bernstein polynomial pk = (1-x/d)^(k*k)
lemma deg_pk (k:ℕ) (d:ℝ)
  : polynomial.nat_degree 
    ((polynomial.C 1- polynomial.C(1/d) * polynomial.X)^(k*k))
      ≤ k*k 
:= begin
    simp,
    have : polynomial.nat_degree (polynomial.C 1- polynomial.C (d⁻¹) * polynomial.X) ≤ 1,
    {
      have h₂ := polynomial.nat_degree_add_le (polynomial.C 1) (- polynomial.C (d⁻¹) * polynomial.X),
      simp at h₂,
      have : (polynomial.C (d⁻¹) * polynomial.X).nat_degree ≤ polynomial.X.nat_degree
        := polynomial.nat_degree_C_mul_le _ _,
      simp at this,
      simp,
      exact le_trans h₂ this,
    },
    calc k*k * polynomial.nat_degree (1- polynomial.C(d⁻¹) * polynomial.X) ≤ k * k * 1 : mul_le_mul_left' this (k * k)
        ... = k*k : mul_one (k * k),
end



lemma pk_nonneg (k:ℕ) (d:ℝ) 
    (dge1: d ≥ 1)
    : ∀ (x : ℝ), x ∈ set.interval 0 d → 0 ≤  (1 - d⁻¹ * x) ^ (k * k) 
:= begin 
    intros x xinint,
    have : 0 ≤ 1 - d⁻¹ * x,
    {
      simp,
      unfold interval at xinint,
      have :  max 0 d = d  := max_eq_right (by linarith),
      rw this at xinint,
      have :  min 0 d = 0  := min_eq_left (by linarith),
      rw this at xinint,
      unfold Icc at xinint,
      have h₁: d⁻¹ * x ≤ d⁻¹*d :=  mul_le_mul_of_nonneg_left 
                      xinint.2 (le_of_lt (inv_pos.mpr (by linarith))),
      have: d≠ 0 := by linarith,
      rw (inv_mul_cancel this) at h₁,
      exact h₁,
    },
    exact pow_nonneg this (k * k),
end


lemma pk_le_1 (k:ℕ) (d:ℝ) 
  (dge1: d ≥ 1)
  : ∀ (x : ℝ), x ∈ set.interval 0 d → (1 - d⁻¹ * x) ^ (k * k)  ≤ 1
:= begin
  intros x xinint,
  unfold interval at xinint,
  have :  max 0 d = d  := max_eq_right (by linarith),
  rw this at xinint,
  have :  min 0 d = 0  := min_eq_left (by linarith),
  rw this at xinint,
  have h₀: (1-d⁻¹*x) ≤ 1,
  {
    simp,
    have: d⁻¹ ≥ 0 := inv_nonneg.mpr (by linarith),
    exact mul_nonneg this xinint.1,
  },
  have h₁: 0 ≤ 1 - d⁻¹ * x,
    {
      simp,
      unfold Icc at xinint,
      have h₁: d⁻¹ * x ≤ d⁻¹*d :=  mul_le_mul_of_nonneg_left 
                      xinint.2 (le_of_lt (inv_pos.mpr (by linarith))),
      have: d≠ 0 := by linarith,
      rw (inv_mul_cancel this) at h₁,
      exact h₁,
    },
  exact pow_le_one (k * k) h₁ h₀,
end

end pk_ineq



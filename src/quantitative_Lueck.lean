/-
Copyright (c) 2022 Clara Löh and Matthias Uschold. All rights reserved.
Released under Apache 2.0 license as described in the file LICENSE.txt.
Authors: Clara Löh and Matthias Uschold.
-/

/- Here, we formalise the quantitative 
Lueck theorem, using very general assumptions
(on traced algebras and spectral measures)

-/
import .quantitative_Lueck_lemmas
import data.polynomial.degree.definitions

open tactic
open classical
open traced_algebra 
open measure_theory
open set
open real
open interval_integral

section   

/- hypotheses needed for the Lueck approximation theorem-/

variables 
{matrix_ring: Type*} [ring matrix_ring]
[tr_alg: traced_algebra ℝ matrix_ring] --the matrix is an algebra with trace over ℝ 
(A: matrix_ring) 
-- A is an element of the matrix ring

--we have a spectral measure μ 
{d:ℚ} (d_ge1: (d:ℝ)≥ 1)
(μ: measure ℝ) [finite_measure μ]
(μ_spec_meas: is_spectral_measure matrix_ring A μ d)
--μ has a logarithmic bound - we just express the case 1/k and the interval (0,1/k] 
{c:ℚ} (c_pos: c > 0)
(μ_log_bound: ∀(k: ℕ), k≥ 2 →  (μ (Ioc 0 (k:ℝ)⁻¹)).to_real ≤ (c / (log k)))
--μ has a total measure
{t:ℚ} (t_pos: t≥ 0)
(μ_tot: (μ (Ioc 0 d)).to_real ≤ t) 

/-similarly, we have the traced algebras matrix_ring_quot with their spectral measures -/
(matrix_ring_quot: ℕ → Type*) [Π n, ring (matrix_ring_quot n)]
[tr_alg_quot : Π n, traced_algebra ℝ (matrix_ring_quot n)]
(μquot: ℕ → measure ℝ) [∀ n, finite_measure (μquot n)]
--μquot n also has a total measure
(μquot_tot: ∀ n, ennreal.to_real((μquot n) (Ioc 0 d)) ≤ t)

-- then, we have the projections from the matrix ring to its quotient
(proj: Π  n:ℕ , matrix_ring → matrix_ring_quot n)

-- we can define Aquot, which is the projection of A into the quotient
local notation `Aquot` n := (proj n) A

variables
-- now, μquot are spectral measures for Aquot
(μquot_spec_meas: ∀n, is_spectral_measure (matrix_ring_quot n) (Aquot n) (μquot n) d)
-- also this measure has a logarithmic bound
(μquot_log_bound: ∀(n:ℕ) (k: ℕ), k≥ 2 →  ((μquot n) (Ioc 0 (k:ℝ )⁻¹)).to_real ≤ (c / (log k)))

-- on polynomials of degree ≤ n^2, traces coincide
(tr_coincide: ∀ (n:ℕ)  (p: polynomial ℝ) , polynomial.nat_degree p ≤ n*n →
    traced_algebra.tr (polynomial.eval₂ algebra.to_ring_hom A p)
    = @traced_algebra.tr ℝ (matrix_ring_quot n) _ _ (tr_alg_quot n)
        (polynomial.eval₂ algebra.to_ring_hom (Aquot n) p))
-- for all n ∈ ℕ and polynomials p with deg p ≤ n*n, 
-- we have tr (p(A)) = tr (p(A_n))


/-
  Lemmas to prove the quantitative Lueck theorem

-/



include tr_coincide μ_spec_meas μquot_spec_meas

/-for polynomials of degree ≤ n*n, integration coincides-/
lemma poly_integration_coincide
  : ∀ (n:ℕ)  (p: polynomial ℝ) , polynomial.nat_degree p ≤ n*n → 
      ∫(x:ℝ) in Icc 0 d,  (λ (x : ℝ), polynomial.eval x p) x ∂μ 
    = ∫(x:ℝ) in Icc 0 d,  (λ (x : ℝ), polynomial.eval x p) x ∂(μquot n)
:= begin 
  assume (n:ℕ),
  assume (p: polynomial ℝ),
  assume (pdegle :  polynomial.nat_degree p ≤ n*n),
  
  calc ∫(x:ℝ) in Icc 0 d,  (λ (x : ℝ), polynomial.eval x p) x ∂μ
      = traced_algebra.tr (polynomial.eval₂ algebra.to_ring_hom A p)
                : μ_spec_meas p
  ... = @traced_algebra.tr ℝ (matrix_ring_quot n) _ _ (tr_alg_quot n)
            (polynomial.eval₂ algebra.to_ring_hom (Aquot n) p)
               : tr_coincide n p pdegle
  ... = ∫(x:ℝ) in Icc 0 d, (λ (x : ℝ), polynomial.eval x p) x ∂(μquot n)
              : (μquot_spec_meas n p).symm,
end

omit tr_coincide μ_spec_meas μquot_spec_meas


lemma lueck_ineq_right (k:ℕ)  {d:ℝ} 
  (dge1: d ≥ 1)
  (μ₁ : measure ℝ) [locally_finite_measure μ₁]
  {t:ℝ}
  (μ₁_tot: ennreal.to_real(μ₁ (Ioc 0 d)) ≤ t)
  -- the integral over the interval (1/k, d] can be bounded from above by the term on the RHS
  : ∫ (x : ℝ) in (↑k)⁻¹..d, (1 - d⁻¹ * x) ^ (k * k) ∂μ₁ ≤ t * (1 - (↑k * d)⁻¹) ^ (k * k)
:= begin
  -- the polynomial function is integrable
  have pkint_right : interval_integrable (λ x, (1 - d⁻¹ * x) ^ (k * k)) μ₁ (k:ℝ)⁻¹ d
  := pk_function_is_integrable_on μ₁ (k:ℝ)⁻¹ d k d,
  -- let g be the following constant function 
  let g : ℝ → ℝ := (λ x, (1 - (k * d)⁻¹) ^ (k * k)),
  -- g is integrable and non-negative
  have gint_left: interval_integrable g μ₁ (0:ℝ) (k:ℝ)⁻¹
        :=continuous.interval_integrable continuous_const (0:ℝ) (k:ℝ)⁻¹,
  have gint_right: interval_integrable g μ₁ (k:ℝ)⁻¹ d 
        := continuous.interval_integrable continuous_const (k:ℝ)⁻¹ d,
  have gnonneg: (1 - ((k:ℝ) * d)⁻¹) ^ (k * k) ≥ 0 
        := pow_nonneg (one_kd_inv_nonneg dge1) (k * k),
  have g_nonneg_int : ∀ u, u ∈ interval (0:ℝ) (k:ℝ)⁻¹ → 0 ≤ g u,
  {
      intros u uinint,
      change 0 ≤ (1 - ((k:ℝ) * d)⁻¹) ^ (k * k),
      exact gnonneg,
  },

  calc ∫ (x : ℝ) in (↑k)⁻¹..d, (1 - d⁻¹ * x) ^ (k * k) ∂μ₁
      ≤  ∫ (x : ℝ) in (↑k)⁻¹..d, (1 - (k * d)⁻¹) ^ (k * k) ∂μ₁
            : interval_integral.integral_mono_on pkint_right gint_right (kinv_led dge1) 
                          (poly_in_right_interval dge1)
  ... = 0 + ∫ (x : ℝ) in (↑k)⁻¹..d, (1 - (k * d)⁻¹) ^ (k * k) ∂μ₁
            : by simp
  ... ≤ ∫ (x : ℝ) in 0..(↑k)⁻¹, (1 - (k * d)⁻¹) ^ (k * k) ∂μ₁
        + ∫ (x : ℝ) in (↑k)⁻¹..d, (1 - (k * d)⁻¹) ^ (k * k) ∂μ₁ 
            : by simp [interval_integral.integral_nonneg kinv_nonneg g_nonneg_int]
  ... = ∫ (x : ℝ) in (0:ℝ)..d, (1 - ((k:ℝ) * d)⁻¹) ^ (k * k) ∂μ₁
            : interval_integral.integral_add_adjacent_intervals gint_left gint_right
  ... ≤ t * (1 - ((k:ℝ) * d)⁻¹) ^ (k * k)
            : by {
                rw interval_integral.integral_const',
                have : Ioc d 0 = ∅ := set.Ioc_eq_empty (by linarith),
                rw this,
                rw measure_theory.measure_empty,
                simp,
                exact mul_mono_nonneg gnonneg μ₁_tot,
              },
end






--The following calculation is the main calculation in the quantitative 
-- version of Luecks approximation theorem. Because the same calculation 
-- is essentially done twice, we state this more general lemma, which
-- then is applied twice.
lemma lueck_quantitative_calc {k:ℕ} 
  (kge2: k≥ 2) 
  (dge1: (d:ℝ) ≥ 1)
  {μ₁ μ₂ : measure ℝ} [locally_finite_measure μ₁] [locally_finite_measure μ₂]
  (μ₁_log_bound: ∀(k: ℕ), k≥ 2 → ennreal.to_real (μ₁ (Ioc 0 (k:ℝ)⁻¹)) ≤ (c / (log k)))
  (μ₁_tot: ennreal.to_real(μ₁ (Ioc 0 d)) ≤ t)
  (coincide_small_poly: 
    ∀ (p: polynomial ℝ) , polynomial.nat_degree p ≤ k*k → 
       ∫(x:ℝ) in Icc 0 d,  (λ (x : ℝ), polynomial.eval x p) x ∂μ₁ 
     = ∫(x:ℝ) in Icc 0 d,  (λ (x : ℝ), polynomial.eval x p) x ∂μ₂)
  : (μ₂ {0}).to_real ≤  (μ₁ {0}).to_real + c/(log k) + t* (1- 1/(k*d))^(k*k) 
:= begin
  let pk : polynomial ℝ := (polynomial.C 1- polynomial.C(1/(d:ℝ)) * polynomial.X)^(k*k),

  have int_left_le_log_bound: ∫ x in 0 .. (k:ℝ)⁻¹, (λ (x : ℝ), polynomial.eval x pk) x ∂μ₁
                          ≤ c/(log k),
  { 
    have : ∀ (x : ℝ), x ∈ set.interval 0 (d:ℝ) → (λ (x : ℝ), polynomial.eval x pk) x  ≤ 1 
          := by  {simp, exact pk_le_1 k d dge1,},
    calc ∫(x:ℝ) in 0 .. (k:ℝ)⁻¹, (λ (x : ℝ), polynomial.eval x pk) x ∂μ₁ 
        ≤ (μ₁ (Ioc 0 (k:ℝ)⁻¹)).to_real
          : int_function_btween_1 k d dge1 μ₁ 
              (poly_function_is_integrable_on μ₁ 0 (k:ℝ)⁻¹ pk) this
    ... ≤ c / log k
          : by simp only [μ₁_log_bound k kge2],
  },

  calc (μ₂ {0}).to_real 
      = (μ₂ {0}).to_real + 0
          : by simp 
  ... ≤ (μ₂ {0}).to_real +  ∫ (x:ℝ) in 0..d, (λ (x : ℝ), polynomial.eval x pk) x ∂μ₂
          : by simp[interval_integral.integral_nonneg (by linarith) (pk_nonneg k d dge1)]
  ... = ∫(x:ℝ) in Icc 0 d, (λ (x : ℝ), polynomial.eval x pk) x ∂μ₂
          : by {
              rw int_Icc_vs_Ioc (d_nonneg dge1) μ₂ (poly_function_is_integrable_Icc μ₂ 0 d pk),
              simp,
            }
  ... = ∫(x:ℝ) in Icc 0 d, (λ (x : ℝ), polynomial.eval x pk) x ∂μ₁
          : eq.symm (coincide_small_poly pk (deg_pk k d))
          -- follows from the assumption coincide_small_poly
  ... = (μ₁ {0}).to_real + ∫(x:ℝ) in 0..d, (λ (x : ℝ), polynomial.eval x pk) x ∂μ₁
          : by {
              rw int_Icc_vs_Ioc (d_nonneg dge1) μ₁ (poly_function_is_integrable_Icc μ₁ 0 d pk),
              simp,
            } 
  ... = (μ₁ {0}).to_real + ∫(x:ℝ) in 0..(k:ℝ)⁻¹, (λ (x : ℝ), polynomial.eval x pk) x ∂μ₁
                         + ∫(x:ℝ) in (k:ℝ)⁻¹..d, (λ (x : ℝ), polynomial.eval x pk) x ∂μ₁
          : by {
              rw add_assoc,
              congr' 1,
              rw interval_integral.integral_add_adjacent_intervals 
                              (poly_function_is_integrable_on μ₁ 0 (k:ℝ)⁻¹ pk)
                              (poly_function_is_integrable_on μ₁ (k:ℝ)⁻¹ d pk),
            }
  ... ≤ (μ₁ {0}).to_real + c/(log k)
                         + ∫(x:ℝ) in (k:ℝ)⁻¹..d, (λ (x : ℝ), polynomial.eval x pk) x ∂μ₁
          : by linarith only [int_left_le_log_bound]
  ... ≤ (μ₁ {0}).to_real + c/(log k) + t* (1- 1/(k*d))^(k*k)
          : by simp [lueck_ineq_right k dge1 μ₁ μ₁_tot], 
          -- this inequality is treated as a separate lemma above
  
end






/- the following theorem is the main theorem of this file,
namely the quantitative version of the Lueck approximation theorem-/
include μ_log_bound μquot_log_bound  μ_tot μquot_tot d_ge1
        matrix_ring A μ_spec_meas matrix_ring_quot proj μquot_spec_meas tr_coincide

theorem lueck_quantitative {k:ℕ} 
  (kge2: k≥ 2) 
  : abs ( (μ {0}).to_real -  ((μquot k) {0}).to_real) 
  ≤ t* (1- 1/(k*d))^(k*k) + c/(log k)
:= begin
  -- first, recall that polynomial integration coincides
  have poly_int : ∀ (p: polynomial ℝ) , polynomial.nat_degree p ≤ k*k → 
                ∫(x:ℝ) in Icc 0 d,  
                        (λ (x : ℝ), polynomial.eval x p) x ∂μ
                = ∫(x:ℝ) in Icc 0 d,  
                        (λ (x : ℝ), polynomial.eval x p) x ∂(μquot k) 
      := poly_integration_coincide A μ μ_spec_meas matrix_ring_quot μquot proj μquot_spec_meas tr_coincide k,            
  
  -- it suffices to prove the following two inequalities
  suffices : (μquot k {0}).to_real        ≤  (μ {0}).to_real + c/(log k) + t* (1- 1/(k*d))^(k*k)
          ∧ (μ {0}).to_real   ≤  (μquot k {0}).to_real + c/(log k) + t* (1- 1/(k*d))^(k*k),
      { apply abs_le.mpr; split; linarith,},

  -- this goal is then shown by applying lueck_quantitative_calc twice 
  exact ⟨lueck_quantitative_calc kge2 d_ge1 μ_log_bound μ_tot poly_int,
        lueck_quantitative_calc  kge2 d_ge1 (μquot_log_bound k) (μquot_tot k) 
                                                                (λ p hp, (poly_int p hp).symm)⟩,
end

end 
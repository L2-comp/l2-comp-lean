/-
Copyright (c) 2022 Clara Löh and Matthias Uschold. All rights reserved.
Released under Apache 2.0 license as described in the file LICENSE.txt.
Authors: Clara Löh and Matthias Uschold.
-/

import tactic 
import measure_theory.integration 
import measure_theory.interval_integral
import topology.algebra.polynomial
open classical
open set 
open algebra
open interval_integral
open measure_theory


/- In this file, we will introduce traced algebras, i.e.\
algebras with a trace map back to the base ring
 * We will  moreover introduce spectral measures: a spectral measure of 
 (A:matrix_ring) is a measure μ such that for all polynomials p∈ R[X],
 we have:
 the integral of p w.r.t. μ  is equal to tr(p(A))

-/

section traced_algebra

class traced_algebra 
  (R: Type*) 
  (matrix_ring: Type*) [comm_semiring R] [semiring matrix_ring]  
  extends algebra R matrix_ring
  := (tr: matrix_ring →  R)
end traced_algebra


section spectral_measure

variables  (matrix_ring: Type*) 
[ring matrix_ring] [algebra real matrix_ring] [traced_algebra real matrix_ring]  

def is_spectral_measure 
  (A : matrix_ring) 
  (μ : measure ℝ) 
  (d : ℝ) 
  : Prop 
:= ∀ (p : polynomial ℝ),  
    ∫ (x : ℝ) in Icc 0 d, (λ (x : ℝ), polynomial.eval x p) x ∂μ 
  = traced_algebra.tr (polynomial.eval₂  algebra.to_ring_hom A p)
--  integral of p(x) from 0 to d = tr (p(A))
end spectral_measure
/-
Copyright (c) 2022 Clara Löh and Matthias Uschold. All rights reserved.
Released under Apache 2.0 license as described in the file LICENSE.txt.
Authors: Clara Löh and Matthias Uschold.
-/

/- this file defines computable sequences of rational numbers
and then the notions of left-, right- and effective computability. 

The main lemma will be that a number is effectively computable
if and only if it is left and right computable-/

import tactic 
import algebra.floor 
import data.real.basic
import data.rat.basic
import analysis.special_functions.exp_log
import topology.basic
import analysis.special_functions.pow
import .limit_lemmas 

open classical
open set 
open real 



--here it is preferable to define it as of type ℕ → ℝ
-- adjusted: we define sequences that are computable from some
-- oracle sequence in an `elementary' way
inductive el_computable_from 
  : (ℕ → ℝ) → (ℕ → ℝ) → Prop 
  | oracle_computable: ∀(oracle: ℕ → ℝ), el_computable_from oracle oracle
  | el_computable_transitive : ∀ (a b c : ℕ → ℝ), 
        el_computable_from a b → el_computable_from b c → el_computable_from a c
  | inclusion_computable: ∀(oracle: ℕ → ℝ), el_computable_from oracle (λ n, (n:ℝ))
  | constant_computable: ∀(oracle: ℕ → ℝ),∀q:ℚ, el_computable_from oracle (λ n, q)
  | sum_computable: ∀(oracle: ℕ → ℝ),∀{a b:ℕ → ℝ}, 
        el_computable_from oracle a → el_computable_from oracle b → el_computable_from oracle (a+b)
  | product_computable: ∀(oracle: ℕ → ℝ),∀{a b:ℕ → ℝ}, 
        el_computable_from oracle a → el_computable_from oracle b → el_computable_from oracle (λ n, (a n)*(b n))
  | div_computable: ∀(oracle: ℕ → ℝ),∀{a b:ℕ → ℝ}, 
        el_computable_from oracle a → el_computable_from oracle b → el_computable_from oracle (λ n, (a n)/(b n))
  | pow_computable: ∀(oracle: ℕ → ℝ),∀{a b:ℕ → ℝ}, 
        el_computable_from oracle a → el_computable_from oracle b → el_computable_from oracle (λ n, (a n)^(b n))
  | floor_computable : ∀(oracle: ℕ → ℝ),∀{a:ℕ → ℝ}, 
        el_computable_from oracle a → el_computable_from oracle (λ n, floor(a n))
  | log_computable : ∀(oracle: ℕ → ℝ),el_computable_from oracle (λ n, log n)
  | case_distinction_computable: ∀(oracle: ℕ → ℝ),∀{a b:ℕ → ℝ} (N:ℕ), 
        el_computable_from oracle a → el_computable_from oracle b → 
        el_computable_from oracle (λ n, ite (n≤ N) (a n) (b n))
  | max_computable : ∀(oracle: ℕ → ℝ),∀{a b:ℕ → ℝ} (N:ℕ), 
        el_computable_from oracle a → el_computable_from oracle b → 
        el_computable_from oracle (λ n, max (a n) (b n))
  | composition_computable : ∀(oracle: ℕ → ℝ),∀{a b:ℕ → ℝ}, 
        el_computable_from oracle a → el_computable_from oracle b → 
        el_computable_from oracle (λ n, a ((floor (b n)).to_nat))

open el_computable_from

section el_computable_from_lemmas 


lemma sum_computable' 
  {oracle: ℕ → ℝ}
  : ∀{a b:ℕ → ℝ}, el_computable_from oracle a → el_computable_from oracle b → el_computable_from oracle (λ n, a n + b n)
:= begin 
  intros a b ha hb,
  have: a+b = (λ n, a n + b n), {ext, simp},
  exact sum_computable oracle ha hb; rw this,
end 

lemma neg_el_computable_from' {a oracle:ℕ → ℝ} (h:el_computable_from oracle a)
  : el_computable_from oracle(λ n, -(a n))
:= begin
  have := product_computable oracle (constant_computable oracle (-1)) h,
  simp at this,
  exact this,
end

lemma neg_el_computable_from {a oracle:ℕ → ℝ} (h:el_computable_from oracle a)
  : el_computable_from oracle (-a)
:= begin 
  have : (λ n, - a n) = -a, {ext, by simp},
  exact neg_el_computable_from' h;rw this,
end 

lemma diff_el_computable_from {oracle: ℕ → ℝ}
  : ∀{a b:ℕ → ℝ}, el_computable_from oracle a → el_computable_from oracle b → el_computable_from oracle (λ n, a n - b n)
:= begin 
  intros a b ha hb,
  have h₁ := sum_computable' ha (neg_el_computable_from' hb),
  simp at h₁,
  have : (λ n, a n  + - b n) = (λ n, a n - b n),
  {
    ext,
    exact tactic.ring.add_neg_eq_sub (a x) (b x),
  },
  exact h₁;rw this,
end 

lemma inv_el_computable_from {oracle: ℕ → ℝ}
: ∀{ a:ℕ → ℝ }, el_computable_from oracle a → el_computable_from oracle (λ n, (a n)⁻¹)
:= begin
  intros a ha,
  have := div_computable oracle (constant_computable oracle 1) ha, 
  simp at this,
  exact this,
end


lemma prod_nat_cast' {m n : ℕ} : (m:ℝ) * (n:ℝ) = (↑(m*n):ℝ)
:= begin 
  induction m; simp,
end 



lemma floor_to_nat {m: ℕ} 
  : (floor (m:ℝ)).to_nat = m 
:= begin 
  rw ← int.cast_coe_nat m,
  rw floor_coe m,
  exact int.to_nat_coe_nat m,
end 

lemma akk_computable {a oracle: ℕ→ ℝ } (ha: el_computable_from oracle a)
  : el_computable_from oracle (λ k, a (k*k))
:= begin 
  have kk_comp: el_computable_from oracle (λ k, k*k),
  {
    have := product_computable oracle (inclusion_computable _) (inclusion_computable _),
    simp at this,
    exact this,
  },
  have with_floor:= composition_computable oracle ha kk_comp,
  simp at with_floor,
  have: (λ (k:ℕ ), a ((⌊ (k:ℝ) * (k:ℝ)⌋).to_nat)) = (λ k, a (k*k)),
  {
    ext k,
    rw prod_nat_cast',
    rw floor_to_nat,
  },
  rw this at with_floor,
  exact with_floor,
end

end el_computable_from_lemmas

section EC_defs
/-
  Here, we define effective computability,
  left, and right-computability
-/


notation `|` x `|` := abs x

-- effective computability: we can estimate the error
-- adapted: version with oracle

def EC_from (oracle:ℕ → ℝ) (x:ℝ) 
  : Prop 
:= ∃ (q ε : ℕ → ℝ),   
    el_computable_from oracle q 
  ∧ el_computable_from oracle ε 
  ∧ seq_limit ε 0
  ∧ (∀ n, |x - (q n)| ≤ ε n)

--left computability: there is a computable sequence 
-- converging from below
def LC_from (oracle:ℕ → ℝ) (x:ℝ) 
  : Prop 
:= ∃ (q : ℕ → ℝ), 
    el_computable_from oracle q 
  ∧ seq_limit q x 
  ∧ (∀ n, q n ≤ x)

-- and analogously: right computability with a sequence from above
def RC_from (oracle:ℕ → ℝ) (x:ℝ) 
  : Prop 
:= ∃ (q : ℕ → ℝ), 
    el_computable_from oracle q 
  ∧ seq_limit q x 
  ∧ (∀ n, q n ≥ x)


end EC_defs

section examples

--rational numbers are EC
example {oracle:ℕ→ℝ} {q:ℚ} 
  : EC_from oracle q 
:= begin 
  use (λ n, q),
  use (λ n, 0),
  split,
    exact constant_computable oracle q,
  split,
    have := constant_computable oracle 0,
    simp at this,
    exact this,
  split,
  {
    unfold seq_limit,
    exact tendsto_const_nhds,
  },
  simp,  
end 

end examples

section EC_lemmas 


/- here, we prove some easy lemmas about EC, LC and RC-/
lemma EC_of_ineq_kge2 
  {oracle q ε : ℕ → ℝ} {x:ℝ}
  (hq: el_computable_from oracle q) 
  (hε: el_computable_from oracle ε)
  (hlim: seq_limit ε 0) 
  (ineq: ∀ n≥ 2, |x - (q n)| ≤ ε n)
  : EC_from oracle x 
:= begin 
    use q,
    let ε': ℕ → ℝ := (λ n, ite (n≤ 1) (max (ceil (|x-q 0|)) (ceil (|x-q 1|))) (ε n)),
    use ε',
    split,
      exact hq,
    split,
      have hc := constant_computable oracle (max (ceil (|x-q 0|)) (ceil (|x-q 1|))),
      simp at hc,
      exact case_distinction_computable oracle 1 hc hε,
    split,
    {
      unfold seq_limit,
      unfold seq_limit at hlim,
      rw topological_space.seq_tendsto_iff,
      rw topological_space.seq_tendsto_iff at hlim,
      intros U zeroinU isopenU,
      specialize hlim U zeroinU isopenU,
      cases hlim with N,
      use max N 2,
      intros n ngemax,
      have : n ≥ 2 := le_of_max_le_right ngemax,
      have : ε' n = ε n,
      {
        dsimp [ε'],
        simp [this],
        intro,
        linarith,
      },
      rw this,
      have : n ≥ N := le_of_max_le_left ngemax,
      exact hlim_h n this,
    },
    {
      intro n,
      by_cases  n=0 ∨ n=1,
      {
        cases h,
          simp [h],
          dsimp [ε'],
          simp,
          left,
          exact le_ceil (|x - q 0|),
        simp [h],
          dsimp [ε'],
          have : 1 ≤ 1 := rfl.ge,
          rw if_pos this,
          simp,
          right,
          exact le_ceil (|x - q 1|),
      },
      {
        push_neg at h,
        have h': n ≥ 2,
        {
          have: n > 1 := nat.one_lt_iff_ne_zero_and_ne_one.mpr h,
          exact nat.succ_le_iff.mpr this,
        },
        specialize ineq n h',
        dsimp [ε'],
        have: ¬ n ≤ 1 := by linarith,
        rw if_neg this,
        exact ineq,
      },
    },
end 





lemma neg_LC_of_RC {oracle: ℕ → ℝ} {x:ℝ} (x_RC : RC_from oracle x) 
  : LC_from oracle (-x)
:= begin 
  rcases x_RC with ⟨ q, ⟨h₁, ⟨h₂, h₃⟩⟩⟩,
  use -q,
  split,
  show  el_computable_from oracle (-q), 
      exact neg_el_computable_from h₁,
  split,
  show ∀ n, (-q) n ≤ -x,
  {
    intro n,
    have : (-q) n = - (q n) := by simp,
    by linarith [h₃ n],
  },
  show seq_limit (-q) (-x),
  {
    unfold seq_limit,
    unfold seq_limit at h₂,
    exact filter.tendsto.neg h₂,
  },
end 

lemma RC_of_neg_LC {oracle: ℕ → ℝ} {x:ℝ} (neg_x_LC: LC_from oracle (-x)) 
  : RC_from oracle x 
:= begin 
    rcases neg_x_LC with ⟨ q, ⟨h₁, ⟨h₂, h₃⟩⟩⟩,
    use (-q),
    split,
    show  el_computable_from oracle (-q), 
      exact neg_el_computable_from h₁,
    split,
    show ∀ n, (-q) n ≥ x,
    {
      intro n,
      have : (-q) n = - (q n) := by simp,
      by linarith [h₃ n],
    },
    show seq_limit (-q) x,
    {
      unfold seq_limit,
       unfold seq_limit at h₂,
      have := filter.tendsto.neg h₂,
      simp at this,
      have h': (λ n, - q n) = -q,{ext,simp,},
      rw h' at this,
      exact this,
    },
  end

lemma RC_iff_neg_LC {oracle: ℕ → ℝ} {x:ℝ}
  : RC_from oracle x ↔ LC_from oracle (-x)
:= iff.intro (λ h, neg_LC_of_RC h) (λ h, RC_of_neg_LC h)

lemma neg_EC {oracle: ℕ → ℝ} {x:ℝ} (h:EC_from oracle x) 
  : EC_from oracle (-x)
:= begin 
  rcases h with ⟨q, ε, h₁, h₂,h₃,h₄⟩,
  use (-q),
  use ε,
  split,
  show el_computable_from oracle (-q),
    exact neg_el_computable_from h₁,
  split,
    exact h₂,
  split,
    exact h₃,
  intro n,
  simp,
  rw abs_sub (q n) x,
  exact h₄ n,
end 

lemma convergence_of_EC_cond 
  {x:ℝ} 
  {q e :ℕ → ℝ }
  (hε: seq_limit e 0)
  (bound: ∀ n, |x - (q n)| ≤ e n)
  : seq_limit q x 
:= begin 
  rw seq_limit_classical at *,
  intros ε εpos,
  specialize hε ε εpos,
  cases hε with N,
  use N,
  intros n ngeN,
  specialize hε_h n ngeN,
  specialize bound n,
  simp at hε_h,
  have : e n ≥ 0 := le_trans (abs_nonneg (x - q n)) bound,
  have : |e n|  = e n := abs_eq_self.mpr this,
  rw this at hε_h,
  rw abs_sub  (q n) x,
  linarith,
end 


lemma RC_of_EC 
  {oracle: ℕ → ℝ} {x:ℝ} 
  (h: EC_from oracle x) 
  : RC_from oracle x
:= begin 
    rcases h with ⟨q, ε, ⟨h₁, h₂, h₃,h₄⟩⟩, 
    use (q+ε),
    split,
      exact sum_computable oracle h₁ h₂,
    split,
    {
      have: x= x+0 := self_eq_add_right.mpr rfl,
      rw this,
      have q_limit:= convergence_of_EC_cond h₃ h₄,
      exact sum_seq_limit q_limit h₃,
    },
    {
      intro n,
      specialize h₄ n,
      simp,
      linarith [le_of_abs_le h₄],
    },
end

lemma LC_of_EC 
  {oracle: ℕ → ℝ} {x:ℝ} 
  (h: EC_from oracle x) 
  : LC_from oracle x
:= begin 
  have : x = - (-x) := eq_neg_of_eq_neg rfl,
  rw this,
  rw ← RC_iff_neg_LC,
  have h' : EC_from oracle (-x) := neg_EC h,
  exact RC_of_EC h',
end

lemma EC_of_LC_RC 
  {oracle: ℕ → ℝ} {x:ℝ} 
  (hl: LC_from oracle x) 
  (hr: RC_from oracle x) 
  : EC_from oracle x 
:= begin 
    unfold LC_from at hl,
    unfold RC_from at hr,
    unfold EC_from,
    rcases hl with ⟨ql, hl₁, hl₂, hl₃⟩,
    rcases hr with ⟨qr, hr₁, hr₂, hr₃⟩,
    use ql,
    use (λ n, qr n - ql n),
    split,
      exact hl₁,
    split,
      exact diff_el_computable_from hr₁ hl₁,
    split,
    {
      have : seq_limit (qr - ql) (x-x):= diff_seq_limit hr₂ hl₂,
      simp at this,
      exact this,
    },
    {
      intro n,
      specialize hl₃ n,
      specialize hr₃ n,
      rw abs_le,
      split; linarith,
    },
end

lemma EC_iff_LC_RC {oracle: ℕ → ℝ} {x:ℝ} 
  : EC_from oracle x ↔ LC_from oracle x ∧ RC_from oracle x 
:= iff.intro (λ h, and.intro (LC_of_EC h) (RC_of_EC h))
              (λ h, EC_of_LC_RC h.1 h.2)



--LC_from is transitive
-- (actually this is also true for RC_from and EC_from but we only need it for LC)
lemma LC_from_transitive 
  {oracle₁ oracle₂ : ℕ → ℝ } {x:ℝ}
  (h_oracle: el_computable_from oracle₁ oracle₂)
  (hx : LC_from oracle₂ x)
  : LC_from oracle₁ x 
:= begin 
  change ∃ (q : ℕ → ℝ), el_computable_from oracle₂ q 
                    ∧ seq_limit q x 
                    ∧ (∀ n, q n ≤ x) 
          at hx,
  --pick such a q,
  rcases hx with ⟨q, hx⟩,
  --we have to show the following
  change ∃ (q : ℕ → ℝ), el_computable_from oracle₁ q 
                    ∧ seq_limit q x 
                    ∧ (∀ n, q n ≤ x),
  --we use the same q
  use q,
  --q is also computable from oracle₁
  have : el_computable_from oracle₁ q := el_computable_transitive _ _ _ h_oracle hx.1,
  --we then just reassemble
  exact and.intro this hx.2,
end 

end EC_lemmas


 
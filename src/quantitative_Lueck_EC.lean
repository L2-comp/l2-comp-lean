/-
Copyright (c) 2022 Clara Löh and Matthias Uschold. All rights reserved.
Released under Apache 2.0 license as described in the file LICENSE.txt.
Authors: Clara Löh and Matthias Uschold.
-/

import tactic
import .quantitative_Lueck
import .computable_sequences

/- in this file, we will prove the computability
of L^2-Betti numbers from the quantitative Lueck 
approximation theorem. This mimics the case that the group is 
finitely presented, residually finite

We will use the quantitative Lueck theorem 
and its assumptions-/

open classical 
open traced_algebra
open measure_theory
open set
open real


/- hypotheses needed for the Lueck approximation theorem-/

variables 
{matrix_ring: Type*} [ring matrix_ring]
[tr_alg: traced_algebra ℝ matrix_ring] --the matrix is an algebra with trace over ℝ 
(A: matrix_ring) 
-- A is an element of the matrix ring

--we have a spectral measure μ 
{d:ℚ} (d_ge1: (d:ℝ)≥ 1)
(μ: measure ℝ) [finite_measure μ]
(μ_spec_meas: is_spectral_measure matrix_ring A μ d)
--μ has a logarithmic bound - we just express the case 1/k and the interval (0,1/k] 
{c:ℚ} (c_pos: c > 0)
(μ_log_bound: ∀(k: ℕ), k≥ 2 →  (μ (Ioc 0 (k:ℝ)⁻¹)).to_real ≤ (c / (log k)))
--μ has a total measure
{t:ℚ} (t_pos: t≥ 0)
(μ_tot: ennreal.to_real(μ (Ioc 0 d)) ≤ t) 

/-similarly, we have the traced algebras matrix_ring_quot with their spectral measures -/
(matrix_ring_quot: ℕ → Type*) [Π n, ring (matrix_ring_quot n)]
[tr_alg_quot : Π n, traced_algebra ℝ (matrix_ring_quot n)]
(μquot: ℕ → measure ℝ) [∀ n, finite_measure (μquot n)]
--μquot n also has a total measure
(μquot_tot: ∀ n, ((μquot n) (Ioc 0 d)).to_real ≤ t)

-- then, we have the projections from the matrix ring to its quotient
(proj: Π  n:ℕ , matrix_ring → matrix_ring_quot n)

-- we can define Aquot, which is the projection of A into the quotient
local notation `Aquot` n := (proj n) A

variables
-- now, μquot are spectral measures for Aquot
(μquot_spec_meas: ∀n, is_spectral_measure (matrix_ring_quot n) (Aquot n) (μquot n) d)
-- also this measure has a logarithmic bound
(μquot_log_bound: ∀(n:ℕ) (k: ℕ), k≥ 2 → ennreal.to_real ((μquot n) (Ioc 0 (k:ℝ )⁻¹)) ≤ (c / (log k)))

-- on polynomials of degree ≤ n^2, traces coincide
(tr_coincide: ∀ (n:ℕ)  (p: polynomial ℝ) , polynomial.nat_degree p ≤ n*n →
              traced_algebra.tr (polynomial.eval₂ algebra.to_ring_hom A p)
            = @traced_algebra.tr ℝ (matrix_ring_quot n) _ _ (tr_alg_quot n)
            (polynomial.eval₂ algebra.to_ring_hom (Aquot n) p))
-- for all n ∈ ℕ and polynomials p with deg p ≤ n*n, 
-- we have tr (p(A)) = tr (p(A_n))





section preparation 

open el_computable_from

lemma prod_nat_cast {m n : ℕ} 
  : (m:ℝ) * (n:ℝ) = (↑(m*n):ℝ)
:= begin 
  induction m; simp,
end 

-- the follwoing sequence is computable (from any oracle) 
--this follows from repeated application of the axioms of computability  
lemma ε_computable {t d c :ℚ} {oracle: ℕ → ℝ }
  : el_computable_from oracle  
    (λ (k : ℕ), t * (1 - 1 / (↑k * d)) ^ (k * k) + c / log ↑k)
:= begin 
  apply sum_computable',
    apply product_computable,
      apply constant_computable,
      have: (λ (n : ℕ), (1 - 1 / (↑n * (d:ℝ))) ^ (n * n))
                = (λ (n : ℕ), (1 - 1 / (↑n * ↑d)) ^ ((n * n):ℝ)),
      {
        ext (n:ℕ),
        rw prod_nat_cast',
        exact (rpow_nat_cast _ _).symm,
      },
      rw this,
      apply pow_computable oracle,
        apply diff_el_computable_from,
          have := constant_computable oracle (1:ℚ),
          finish,
          apply div_computable,
            have := constant_computable oracle (1:ℚ),
            finish,
            apply product_computable,
              apply inclusion_computable,
              apply constant_computable,
        apply product_computable,
          apply inclusion_computable,
          apply inclusion_computable,

    apply div_computable,
      apply constant_computable,
      apply log_computable,
end 





lemma ε_to_zero  
  (c:ℚ) {t d :ℚ} 
  (dge1: (d:ℝ) ≥ 1)
  : seq_limit (λ (k : ℕ), t * (1 - 1 / (↑k * d)) ^ (k * k) + c / log ↑k) 0
:= begin 
  have hε₁ : seq_limit (λ (k : ℕ), t * (1 - 1 / (↑k * d)) ^ (k * k)) 0 
            := ε₁_to_zero dge1,
  have hε₂ : seq_limit (λ (k : ℕ), c / log ↑k) 0
            := mul_log_inv_to_zero,
  have := sum_seq_limit hε₁ hε₂,
  simp at *,
  exact this,
end 

end preparation






section main 

include t d c d_ge1 μ μ_spec_meas μ_log_bound
μ_tot μquot μquot_tot μquot_spec_meas μquot_log_bound tr_coincide


local notation `kernels_seq` := (λ n, ((μquot n) {0}).to_real)


--This is the main theorem of this file

theorem EC_of_Lueck 
  : EC_from kernels_seq (μ {0}).to_real
:= begin 
    -- we have the error bound from quantitative_Lueck.lean 
  have error_bound: ∀ k≥ 2, |(μ {0}).to_real - ((μquot k) {0}).to_real| ≤ ↑t * (1 - 1 / (↑k * ↑d)) ^ (k*k) + ↑c / log ↑k 
              := (λ k kge2, lueck_quantitative A d_ge1 μ μ_spec_meas  μ_log_bound
               μ_tot matrix_ring_quot μquot μquot_tot proj (μquot_spec_meas) μquot_log_bound tr_coincide kge2),
  -- and we have that the kernel_seq is computable
  have kernel_seq_computable:     el_computable_from kernels_seq (λ n, ((μquot n) {0}).to_real)
                             := el_computable_from.oracle_computable _,
  -- then the statement follows from a proposition in computable_sequences.lean 
  exact EC_of_ineq_kge2 kernel_seq_computable ε_computable (ε_to_zero c d_ge1) error_bound,
end  


end main 



/-
Copyright (c) 2022 Clara Löh and Matthias Uschold. All rights reserved.
Released under Apache 2.0 license as described in the file LICENSE.txt.
Authors: Clara Löh and Matthias Uschold.
-/

/- in this file, we will prove that the determinant class
conjecture implies the existence of a logarithmic bound 
(and hence also effective computability relative to the 
computability class of the trace map)-/

import tactic
import data.real.ennreal
import .general_bound_LC


open classical 
open traced_algebra
open measure_theory
open set
open real
open el_computable_from


variables 
--the matrix ring is an algebra with trace over R
{matrix_ring: Type*} [ring matrix_ring]
[algebra ℝ matrix_ring]
[tr_alg: traced_algebra ℝ matrix_ring] 
-- A is an element of the matrix ring
(A: matrix_ring) 
--we have a spectral measure μ of A
{d:ℚ} (d_ge1: (d:ℝ)≥ 1)
(μ: measure ℝ) [finite_measure μ]
(μ_spec_meas: is_spectral_measure matrix_ring A μ d)
--that has a total measure at most t
{t:ℚ} (t_pos: t≥ 0)
(μ_tot: ennreal.to_real(μ (Ioc 0 d)) ≤ t) 


/-the polynomials that we want to work with-/

local notation `pk` k  :=  (polynomial.C 1- polynomial.C(1/(d:ℝ)) * polynomial.X)^(k)
local notation `pkk` k  :=  (polynomial.C 1- polynomial.C(1/(d:ℝ)) * polynomial.X)^(k*k)


/-computability will be defined relative to the characteristic sequence
of the matrix A, 
Here, we introduce two variants: tr (pkk (A)) and tr(pk(A))-/
local notation 
`char_seq` := (λ k, traced_algebra.tr (polynomial.eval₂ algebra.to_ring_hom A (pk k)))
local notation 
`char_seq₂` := (λ k, traced_algebra.tr (polynomial.eval₂ algebra.to_ring_hom A (pkk k)))


/-The determinant class conjecture says that log is an integrable function
with respect to μ 
Note: in Lean, we have log 0 = 0, hence integrability on [0,d] and (0,d]
are equivalent-/
variable (determinant_class : integrable log μ)



section 

include A d_ge1 μ_spec_meas μ_tot t_pos determinant_class
/-
  In this situation, there exists an upper bound on μ (0, 1/n], that
  is computable and that tends to zero for n → ∞ 
-/
theorem det_class_exists_log_bound {oracle:ℕ → ℝ}
  : ∃ bound : ℕ → ℝ,
      el_computable_from oracle bound 
    ∧ seq_limit bound (0:ℝ) 
    ∧ ∀(n:ℕ), (μ(Ioc 0 (n:ℝ)⁻¹)).to_real ≤ bound n 
:= begin 
  -- We define q as the following integral (which is finite because log is
  -- integrable)
  let q : ℚ := ceil (- ∫(x:ℝ) in 0..1, log x ∂μ),
  let bound : ℕ → ℝ := (λ (k:ℕ), ite (k≤1) t ((q:ℝ)/(log k))),

  -- we will show that `bound' is such a bound
  suffices : el_computable_from oracle bound ∧ 
              seq_limit bound (0:ℝ) ∧ 
              ∀(n:ℕ), (μ(Ioc 0 (n:ℝ)⁻¹)).to_real ≤ bound n,
         {use bound, exact this,}, -- proof that this suffices
  
  --show all the conjuncts separately
  have h₁: el_computable_from oracle bound 
    := begin
      -- the sequence (q/(log k)) is computable
      have q_over_log_compu : el_computable_from oracle (λ k, (q:ℝ)/(log(k:ℝ)))
            := div_computable oracle (constant_computable _ q) (log_computable _),
      -- also the constant function t is computable
      have t_compu : el_computable_from oracle (λ k, t):= constant_computable oracle t,
      --`case distinctions' are computable
      exact case_distinction_computable _ 1 t_compu q_over_log_compu,
    end,

  have h₂: seq_limit bound (0:ℝ) 
    := begin 
      -- we have that the sequence bound eventually coincides with (q/(log k))
      have coinc: ∀(k:ℕ), k≥ 2 →   ((q:ℝ)*(log k)⁻¹) 
                  =  ite (k≤1) (t:ℝ) ((q:ℝ)*(log k)⁻¹),
        {
          intros k kge2,
          have : ¬ (k≤ 1) := by linarith,
          rw if_neg this,
        },

      --hence, it suffices to show convergence for (q/(log k))
      suffices : seq_limit (λ k, ((q:ℝ)*(log k)⁻¹)) 0,
          {exact eventually_coincide_converges coinc this,},
      -- which is a basic statement about limits
      have := mul_const_converges log_inv_to_zero,
      simp at this,
      exact this,
    end,

  have h₃: ∀(n:ℕ), (μ(Ioc 0 (n:ℝ)⁻¹)).to_real ≤ bound n
    := begin
      assume (k:ℕ),
      -- we distinguish the cases k ≤ 1 and k ≥ 2 

      by_cases hk: k≤ 1,
      -- the case k ≤ 1 is rather trivial, we will not make further comments here
      {
        by_cases hk' : k=0,
        {
          rw hk',
          simp,
          dsimp[bound],
          simp,
          exact t_pos,
        },
        {
          have : k = 1 := le_antisymm hk (nat.succ_le_iff.mpr (zero_lt_iff.mpr hk')),
          rw this,
          dsimp[bound],
          simp,
          rw if_pos,
          have subset : Ioc 0 1 ⊆ Ioc 0 (d:ℝ),
          {
            intros x xin,
            cases xin,
            split,
            exact xin_left,
            exact le_trans xin_right d_ge1,
          },
          have : (μ (Ioc 0 1)) ≤ (μ (Ioc 0 (d:ℝ)))
              := measure_mono subset,
          have : (μ (Ioc 0 1)).to_real ≤ (μ (Ioc 0 (d:ℝ))).to_real,
          {
            have letop₁: (μ (Ioc 0 1)) ≠  ⊤ := ne_of_lt (measure_theory.measure_lt_top μ (Ioc 0 1)),
            have letop₂: (μ (Ioc 0 (d:ℝ))) ≠  ⊤ := ne_of_lt (measure_theory.measure_lt_top μ (Ioc 0 (d:ℝ))),
            exact (ennreal.to_real_le_to_real letop₁ letop₂).mpr this,
          },
          exact le_trans this μ_tot,
          refl,
        },
      },
      -- we now deal with the case k ≥ 2
    have kge2: k ≥ 2 :=  nat.succ_le_iff.mpr (not_le.mp hk),
    -- q is an integral rounded up, so it is greater than or equal to the integral
    have ceil_ge : (q:ℝ) ≥  - ∫(x:ℝ) in 0..1, log x ∂μ,
        {dsimp[q], rw rat.cast_coe_int, exact le_ceil _,},

    have inequality: (q:ℝ) ≥ log (k:ℝ) * (μ (Ioc 0 (k:ℝ)⁻¹)).to_real,
    {
      -- this inequality follows from ceil_ge and some inequalities about
      -- integration of log, proved in general_lemmas.lean
      calc (q:ℝ)  ≥  - ∫(x:ℝ) in 0..1, log x ∂μ         
                                              : ceil_ge
              ... ≥  - ∫(x:ℝ) in 0..(k:ℝ)⁻¹, log x ∂μ    
                                              : log_int_0_1_le_0_kinv determinant_class kge2
              ... ≥ - (log (k:ℝ)⁻¹ * (μ (Ioc 0 (k:ℝ)⁻¹)).to_real)
                                              : log_int_bounded_measure determinant_class
              ... = log (k:ℝ) * (μ (Ioc 0 (k:ℝ)⁻¹)).to_real 
                                              : by rw (k:ℝ).log_inv; simp,
    },

    -- from inequality, we can now deduce the claim 
    calc  (μ (Ioc 0 (k:ℝ)⁻¹)).to_real ≤ (q:ℝ)/(log k) 
                                              : (le_div_iff' (log_pos_of_ge2 kge2)).mpr inequality
                                  ... = ite (k ≤ 1) ↑t (↑q / log ↑k)
                                              : by rw if_neg hk
                                  ... = bound k 
                                              : by dsimp[bound];simp, 
  end,

  -- now, we just assemble all the conjuncts,
  show _,
    exact and.intro h₁ (and.intro h₂ h₃),
end 


 
theorem det_class_implies_EC 
  (determinant_class : integrable log μ)
  : EC_from char_seq (μ {0}).to_real
:= begin 
  --recall the statement of det_class_exists_log_bound
  have : ∃bound : ℕ → ℝ,
              el_computable_from char_seq₂ bound ∧ 
              seq_limit bound (0:ℝ) ∧ 
              ∀(n:ℕ), (μ(Ioc 0 (n:ℝ)⁻¹)).to_real ≤ bound n
            := det_class_exists_log_bound A d_ge1 μ μ_spec_meas t_pos μ_tot 
                determinant_class,
  -- fix such a bound 
   rcases this with ⟨bound, h₁, h₂, h₃⟩,
   
  -- then the statement is given by bound_implies_EC
  exact bound_implies_EC A d_ge1 μ μ_spec_meas μ_tot
    h₁ h₂ h₃,
end 
 
end 
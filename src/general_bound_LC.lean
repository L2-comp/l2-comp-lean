/-
Copyright (c) 2022 Clara Löh and Matthias Uschold. All rights reserved.
Released under Apache 2.0 license as described in the file LICENSE.txt.
Authors: Clara Löh and Matthias Uschold.
-/

/-same hypotheses as in the RC case
but with additional asumption of logarithmic bound
(or more generally, computable bound tending to zero)-/

import tactic
import .quantitative_Lueck
import .general_RC

open classical 
open traced_algebra
open measure_theory
open set
open real
open el_computable_from


/-variables from quantitative_Lueck.lean-/
variables 
{matrix_ring: Type*} [ring matrix_ring]
[algebra ℝ matrix_ring]
[tr_alg: traced_algebra ℝ matrix_ring] 
--the matrix ring is an algebra with trace over R
(A: matrix_ring) 
-- A is an element of the matrix ring

--we have a spectral measure μ of A       
{d:ℚ} (d_ge1: (d:ℝ)≥ 1)
(μ: measure ℝ) [finite_measure μ]
(μ_spec_meas: is_spectral_measure matrix_ring A μ d)

--μ has a total measure
variables {t:ℚ} 
(μ_tot: ennreal.to_real(μ (Ioc 0 d)) ≤ t) 


/-the polynomials that we want to work with
in this case, raised to the power k*k-/

local notation `pk` k  :=  (polynomial.C 1- polynomial.C(1/(d:ℝ)) * polynomial.X)^(k)
local notation `pkk` k  :=  (polynomial.C 1- polynomial.C(1/(d:ℝ)) * polynomial.X)^(k*k)

/-computability relative to tr (pkk (A)) and tr(pk(A))-/
local notation 
`char_seq` := (λ k, traced_algebra.tr (polynomial.eval₂ algebra.to_ring_hom A (pk k)))
local notation 
`char_seq₂` := (λ k, traced_algebra.tr (polynomial.eval₂ algebra.to_ring_hom A (pkk k)))


variables
-- we assume that we have a bound
{bound: ℕ → ℝ}
-- that is computable from char_seq₂
(bound_computable₂: el_computable_from char_seq₂ bound)
--that tends to zero
(bound_to_zero: seq_limit bound (0:ℝ ))
-- such that μ (0, 1/n) ≤ bound n for all n:ℕ 
(bound_bound: ∀(n:ℕ), (μ (Ioc 0 (n:ℝ)⁻¹)).to_real 
                              ≤ bound n)



section computability_relations
/-lemma: we can calculate char_seq₂ from char_seq-/
include  tr_alg A

/-we can compute char_seq₂ from char_seq-/
lemma compu_char_seq₂_from_char_seq 
  : el_computable_from char_seq char_seq₂
:=akk_computable (oracle_computable _)

end computability_relations

section log_bound_implies_LC


include tr_alg

/-
  Under these hypotheses, we have that for all k:ℕ 
  μ {0} ≥ (char_seq₂ k) - (bound k) - (some explicit error term (see below))
-/
lemma LC_inequality 
  (k:ℕ)
  (μ_spec_meas: is_spectral_measure matrix_ring A μ d)
  (d_ge1: (d:ℝ) ≥ 1)
  (bound_bound: ∀(n:ℕ), (μ (Ioc 0 (n:ℝ)⁻¹)).to_real ≤ bound n)
  (μ_tot: ennreal.to_real(μ (Ioc 0 d)) ≤ t) 
  : (μ {0}).to_real ≥ (char_seq₂ k) - (bound k) - t *(1-(k*d)⁻¹)^(k*k)   
:= begin 
  --change to addition rather than substraction
  suffices : (char_seq₂ k) ≤ (μ {0}).to_real + (bound k) + t *(1-(k*d)⁻¹)^(k*k),
  {
    linarith,
  },

  --we prepare some statements for the calculation
  have int_split: ∫ x in 0 .. d, (λ (x : ℝ), polynomial.eval x (pkk k)) x ∂μ
                = ∫ x in 0 .. (k:ℝ)⁻¹, (λ (x : ℝ), polynomial.eval x (pkk k)) x ∂μ
                + ∫ x in (k:ℝ)⁻¹ .. d, (λ (x : ℝ), polynomial.eval x (pkk k)) x ∂μ,
  {
      rw ←interval_integral.integral_add_adjacent_intervals,
      exact poly_function_is_integrable_on μ 0 (k:ℝ)⁻¹ (pkk k),
      exact poly_function_is_integrable_on μ (k:ℝ)⁻¹ d (pkk k),
  },


  -- the integral over the set (0, 1/k] is bounded by the measure of this set
  have left_bound : 
    ∫ x in 0 .. (k:ℝ)⁻¹, (λ (x : ℝ), polynomial.eval x (pkk k)) x ∂μ  ≤ (μ (Ioc 0 (k:ℝ)⁻¹)).to_real,
  {
    -- this comes from the fact that on the whole (0,d), the function is bounded
    -- by 1
    have : ∀ (x : ℝ), x ∈ set.interval 0 (d:ℝ) → 
                (λ (x : ℝ), polynomial.eval x (pkk k)) x  ≤ 1,
    {
      simp,
      exact pk_le_1 k d d_ge1,
    },
    --the result is then given by int_function_btween_1
    exact int_function_btween_1 k d d_ge1 μ 
              (poly_function_is_integrable_on μ 0 (k:ℝ)⁻¹ (pkk k)) this,
  },


  -- the statement now is the following calculation
  calc char_seq₂ k = ∫ x in Icc 0  (d:ℝ), (λ (x : ℝ), polynomial.eval x (pkk k)) x ∂μ
                        : by {
                            simp,
                            have := μ_spec_meas (pkk k),
                            simp at this,
                            exact this.symm,
                          }
        ...       = (μ {0}).to_real 
                    + ∫ x in 0 .. d, (λ (x : ℝ), polynomial.eval x (pkk k)) x ∂μ
                      : by {rw int_Icc_vs_Ioc (d_nonneg d_ge1) μ 
                                (poly_function_is_integrable_Icc μ 0 d (pkk k)),
                            simp,}
        ...       = (μ {0}).to_real 
                    + ∫ x in 0 .. (k:ℝ)⁻¹, (λ (x : ℝ), polynomial.eval x (pkk k)) x ∂μ
                    + ∫ x in (k:ℝ)⁻¹ .. d, (λ (x : ℝ), polynomial.eval x (pkk k)) x ∂μ
                      : by { rw add_assoc; congr' 1;  rw int_split,}
        ...       ≤ (μ {0}).to_real 
                    + (μ (Ioc 0 (k:ℝ)⁻¹)).to_real
                    + ∫ x in (k:ℝ)⁻¹ .. d, (λ (x : ℝ), polynomial.eval x (pkk k)) x ∂μ
                      : by { linarith only [left_bound],}
        ...       ≤ (μ {0}).to_real 
                    + (bound k)
                    + ∫ x in (k:ℝ)⁻¹ .. d, (λ (x : ℝ), polynomial.eval x (pkk k)) x ∂μ
                      : by { linarith only [bound_bound k],}
        ...       = (μ {0}).to_real 
                    + (bound k)
                    + ∫ (x : ℝ) in (↑k)⁻¹..(d:ℝ), (1 - (↑d)⁻¹ * x) ^ (k * k) ∂μ
                      : by {simp,} -- rewrite the polynomial evaluation
        ...       ≤ (μ {0}).to_real 
                    + (bound k) 
                    + t *(1-(k*d)⁻¹)^(k*k)
                      : by {linarith only [lueck_ineq_right k d_ge1 μ μ_tot],
                            --this was already treated in quantitative_Lueck.lean
                        },
end 


-- the sequence converges to μ {0}
lemma LC_limit 
  (μ_spec_meas: is_spectral_measure matrix_ring A μ d)
  (bound_to_zero: seq_limit bound (0:ℝ ))
  (d_ge1: (d:ℝ) ≥ 1)
  : seq_limit (λ k, (char_seq₂ k) - (bound k) -t *(1-(k*d)⁻¹)^(k*k))
      (μ {0}).to_real
:= begin 
  -- this follows by the calculations of the limits of the summands
  simp,
  have first_diff: 
      seq_limit (λ k, (char_seq₂ k) - (bound k)) (μ {0}).to_real,
      {
        have := diff_seq_limit (akk_converges (char_seq_converges A μ_spec_meas d_ge1)) bound_to_zero,
        simp at this,
        simp,
        exact this,
      },
  have second_diff:= diff_seq_limit first_diff (@ε₁_to_zero t d d_ge1),
  simp at second_diff,
  exact second_diff,
end 


-- the sequence on the right hand side is computable from char_seq₂
lemma LC_sequence_computable
{t :ℚ}
(bound_computable: el_computable_from char_seq₂ bound)
: el_computable_from char_seq₂ 
    (λ k, (char_seq₂ k) - (bound k) -t *(1-(k*d)⁻¹)^(k*k))
:= begin 
  -- this follows from the fact that all summands are computable
  -- and that differences are computable

  have h1 : el_computable_from char_seq₂ char_seq₂ 
          := oracle_computable char_seq₂,
  have h2 : el_computable_from char_seq₂ bound 
          := bound_computable,
  have h3 : el_computable_from char_seq₂ 
                      (λ k, t *(1-(k*d)⁻¹)^(k*k) )
          := @LC1_sequence_computable t d char_seq₂,

  
  exact diff_el_computable_from (diff_el_computable_from h1 h2) h3,
end 






include  bound_computable₂ bound_bound
bound_to_zero t 
  μ_tot A   μ_spec_meas d_ge1
--we fix a local notation for the error term
local notation `error_term` k :=  (t:ℚ) *(1-(k*d)⁻¹)^(k*k)
/-theorem: In this situation, μ {0} is left-computable-/
theorem bound_implies_LC 
  : LC_from char_seq₂ (μ {0}).to_real
:= begin
  --we unfold the definition of LC_from 
  change ∃ (q : ℕ → ℝ), 
          el_computable_from char_seq₂ q 
        ∧ seq_limit q (μ {0}).to_real 
        ∧ ∀ (n : ℕ), q n ≤  (μ {0}).to_real,
  --such a sequence is given by char_seq₂ - bound - (some error term)
  
  use (λ (k:ℕ), (char_seq₂ k) - (bound k) - (error_term k)),

  have h1: el_computable_from char_seq₂ (λ (k:ℕ), (char_seq₂ k) - (bound k) - (error_term k))
          :=  LC_sequence_computable A bound_computable₂,

  have h2: seq_limit (λ (k:ℕ), (char_seq₂ k) - (bound k) - (error_term k)) (μ {0}).to_real
          := LC_limit A μ μ_spec_meas bound_to_zero d_ge1,
  

  have h3:  ∀ (n : ℕ), (λ (k:ℕ), (char_seq₂ k) - (bound k) - (error_term k)) n ≤  (μ {0}).to_real,
  {
    assume (n:ℕ),
    exact LC_inequality A μ n μ_spec_meas d_ge1 bound_bound μ_tot,
  },

  show _,
    exact and.intro h1 (and.intro h2 h3),
end 




--this implies that μ {0} is also left_computable from char_seq
theorem bound_implies_LC' 
  : LC_from char_seq (μ {0}).to_real
:= begin
  -- we can compute char_seq₂ from char_seq.
  have h1 : el_computable_from char_seq char_seq₂ := compu_char_seq₂_from_char_seq A,
  -- we have shown above that μ {0} is left computable from char_seq₂
  have h2 :  LC_from char_seq₂ (μ {0}).to_real
      := bound_implies_LC  A d_ge1 μ μ_spec_meas μ_tot
            bound_computable₂ bound_to_zero bound_bound,
  -- the result is then given by a lemma in computable_sequences.lean
  exact LC_from_transitive h1 h2,
end



theorem bound_implies_EC 
  -- we assume that we have a bound
  {bound: ℕ → ℝ}
  -- that is computable from char_seq₂
  (bound_computable₂: el_computable_from char_seq₂ bound)
  --that tends to zero
  (bound_to_zero: seq_limit bound (0:ℝ ))
  -- such that μ (0, 1/n) ≤ bound n for all n:ℕ 
  (bound_bound: ∀(n:ℕ), (μ (Ioc 0 (n:ℝ)⁻¹)).to_real ≤ bound n)
  : EC_from char_seq (μ {0}).to_real
:= begin
  --we have just shown left-computability
  have h_LC : LC_from char_seq (μ {0}).to_real
          := bound_implies_LC' A d_ge1 μ μ_spec_meas μ_tot
            bound_computable₂ bound_to_zero bound_bound,
  -- and we also have right-computability (in general_RC.lean)
  have h_RC : RC_from char_seq (μ {0}).to_real
          := general_RC A d_ge1 μ μ_spec_meas,
  -- left and right-computability then implies effective computability
  exact EC_of_LC_RC h_LC h_RC,  
end 

end log_bound_implies_LC
